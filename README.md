CLUSTERING
===============

Geographical clustering of a set of courses into subareas. Uses the AgglomerativeClustering from the sklearn library.

## INPUTS

This sequencing method takes as an input a set of rides as a csv file (--file, -f option). Those rides must at least have the following attributes (as columns) :

* *id* : The id of the ride.
* *driver_id* : The id of the driver that did the ride.
* *min_lat* : The minimal latitude of the bounding box.
* *min_lng* : The minimal longitude of the bounding box.
* *max_lat* : The maximal latitude of the bounding box.
* *max_lng* : The maximal longitude of the bounding box.

It also takes as input a set of parameters :

* *output_file* : Path to the CSV file that will be outputed with the ride's ids and the correpsonding cluster's ids. Under the "--output_file, -o" option
* *size_cluster_max* : The maximal number of rides per generated cluster. Under the "--size_cluster_max, -s" option, default to 150 rides.
* *distance_threshold* : The maximal distance between two rides inside the same generated cluster. Under the "--distance_threshold, -d" option, default to 10000m.
* *verbosity* : Level of verbosity. Under the "--verbosity, -v" option, default to 0.

## LAUNCH

Must be launched in *Python 3.8.2*.

* Install generategraph
> ```git clone git@bitbucket.org:classncodevteam/generategraph.git```
> ```cd generategraph```
> ```pip install .```

* Install dependencies
>```cd trace_clustering```
>```pip install .```

* Run using the following command.
> ```python main.py -f path/to/rides_file.csv -o path/to/outputed_file.csv -s sizeClusterMax -d distanceThreshold```

## UNIT TESTING
```export PYTHONPATH=trace_clustering; nosetests```


## Clustering Algorithm

The AgglomerativeClustering recursively merges pairs of clusters that minimally increases a given distance. 
In our case, the rides are represented as Bounding Boxes (with min_lat, min_lng, max_lat, max_lng) containing the positions of the ride. We therefore define the distance between two bounding boxes B_1 and B_2 as :

```distance( B_1, B_2 ) = intersection_distance( B_1, B_2 ) * centroid_distance( B_1, B_2 )```

with

```intersection_distance( B_1, B_2 ) = min( area( B_1 ), area( B_2 )) / area( intersection( B_1, B_2 ) )```

and

```centroid_distance( B_1, B_2 ) = Haversine( centroid( B_1 ), centroid( B_2 ) )```