FROM python:3.8.2

## INSTALL DEPENDENCIES
RUN apt-get update && \
    apt-get install -y \
    python-pip \
    git \
    vim

## SET GIT PERMISSIONS
ARG SSH_PRIVATE_REPOSITORY_KEY
WORKDIR /root
RUN mkdir -p ~/.ssh && umask 0077 && echo $SSH_PRIVATE_REPOSITORY_KEY | base64 --decode > /root/.ssh/id_rsa \
    && git config --global url."git@bitbucket.org:".insteadOf https://bitbucket.org/ \
    && ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts

ADD generategraph-revision .


## INSTALL OR-TOOLS
RUN git clone git@bitbucket.org:classncodevteam/generategraph.git && \
    cd /root/generategraph && \
    git pull origin && \
    git checkout $(cat ../generategraph-revision) && \
    pip install . 

## COPY TRACE_CLUSTERING
WORKDIR /root
RUN mkdir trace_clustering

ADD . /root/trace_clustering
RUN pip install --upgrade pip && \
    cd trace_clustering && \
    pip install .

WORKDIR /root/trace_clustering
ENTRYPOINT ["python", "lib/main.py"]
