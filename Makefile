SHELL                := /bin/bash
DOCKER_REGISTRY      := 354792429249.dkr.ecr.eu-west-3.amazonaws.com/rides-clustering
BRANCH               := $(shell git branch | grep \* | cut -d ' ' -f2 | tr _/ -)
COMMIT_SHA           := $(shell git rev-parse HEAD)
COMMIT_SHA_SHORT     := $(shell git rev-parse --short HEAD)
IMG_COMMIT_SHA       := ${DOCKER_REGISTRY}:${COMMIT_SHA}
IMG_COMMIT_SHA_SHORT := ${DOCKER_REGISTRY}:${COMMIT_SHA_SHORT}
COMMIT_TAG           := $(shell git describe --tags)

ifndef SSH_PRIVATE_REPOSITORY_KEY
    ifneq ("$(wildcard ~/.ssh/id_rsa)","")
        SSH_PRIVATE_REPOSITORY_KEY := $(shell cat ~/.ssh/id_rsa | base64 | tr -d '\n')
    else
        $(error SSH_PRIVATE_REPOSITORY_KEY is undefined)
    endif
endif

show:
	@echo ${DOCKER_REGISTRY}
	@echo ${BRANCH}
	@echo ${IMG_COMMIT_SHA}
	@echo ${IMG_COMMIT_SHA_SHORT}
	@echo ${DOCKER_REGISTRY}:${BRANCH}-${COMMIT_SHA}
	@echo ${DOCKER_REGISTRY}:${BRANCH}-${COMMIT_SHA_SHORT}
	@echo ${DOCKER_REGISTRY}:${COMMIT_TAG}

.PHONY: build
build:
	@docker build . \
     --build-arg SSH_PRIVATE_REPOSITORY_KEY=${SSH_PRIVATE_REPOSITORY_KEY} \
     -t ${DOCKER_REGISTRY}
	@docker tag ${DOCKER_REGISTRY} ${IMG_COMMIT_SHA}
	@docker tag ${DOCKER_REGISTRY} ${IMG_COMMIT_SHA_SHORT}
	@docker tag ${DOCKER_REGISTRY} ${DOCKER_REGISTRY}:${BRANCH}-${COMMIT_SHA}
	@docker tag ${DOCKER_REGISTRY} ${DOCKER_REGISTRY}:${BRANCH}-${COMMIT_SHA_SHORT}
	@docker tag ${DOCKER_REGISTRY} ${DOCKER_REGISTRY}:${BRANCH}
	@docker tag ${DOCKER_REGISTRY} ${DOCKER_REGISTRY}:${COMMIT_TAG}

push:
	@docker push ${IMG_COMMIT_SHA}
	@docker push ${IMG_COMMIT_SHA_SHORT}
	@docker push ${DOCKER_REGISTRY}:${BRANCH}-${COMMIT_SHA}
	@docker push ${DOCKER_REGISTRY}:${BRANCH}-${COMMIT_SHA_SHORT}
	@docker push ${DOCKER_REGISTRY}:${BRANCH}
	@docker push ${DOCKER_REGISTRY}:${COMMIT_TAG}

login:
	@aws ecr get-login-password --region eu-west-3 | docker login --username AWS --password-stdin https://354792429249.dkr.ecr.eu-west-3.amazonaws.com

deploy: login build push
