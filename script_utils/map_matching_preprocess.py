
#!/usr/bin/env python

import os
import pytz
import json
import pandas as pd
import numpy as np
import datetime

import generategraph.hist_courses as hc
import generategraph.utils as utils
import generategraph.graph as g



def set_courses_datetimes(courses, tz_courses):
    """@brief Computes the courses start datetimes from the columns "course_date" and "course_hour" 
    and add the new column to the dataframe. 
    Those datetimes represent the hour for which the course was set to start at (does not depend 
    on the driver actions).

    @:param courses The DataFrame of courses to filter containing at least the following columns:
                        "course_date", "course_hour".
    @:param tz_courses The timezone of the courses.
    @return DataFrame of courses with their corresponding start datetimes localized with the correct timezone.
    """
    if courses.empty:
        return courses
    required_columns = {"course_date", "course_hour"}
    if not required_columns.issubset(courses.columns):
        raise TypeError(("The given courses dataframe is missing at least one of the following "
                         "columns : \"course_date\" and \"course_hour\"."))
    if tz_courses == "":
        raise TypeError("You must give a timezone for the courses.")

    courses_datetimes = courses["course_date"] + " " + courses["course_hour"]
    courses["courses_datetimes"] = pd.to_datetime(courses_datetimes)
    courses["courses_datetimes"] = courses["courses_datetimes"].dt.tz_localize(tz_courses, ambiguous='NaT')
    return courses



def get_courses_in_period(courses, min_date, max_date, tz_courses="", log=1):
    """@brief Returns the subset of courses that were set to start during the 
    given period [min_date, max_date].

    @:param courses The DataFrame of courses to filter containing the following columns:
                    * "course_date", "course_hour" OR
                    * "courses_datetimes"
    @:param min_date The minumum datetime of the period.
    @:param max_date The maximum datetime of the period.
    @:param tz_courses The timezone of the courses. Useful if the courses dataframe does 
                      not have a "courses_datetimes" column.
    @:param verbosity Increase level of verbosity. Default to 1.
    @return DataFrame of courses that were supposed to start in the given period.
    """
    if courses.empty:
        return courses
    if "courses_datetimes" not in courses.columns:
        courses = set_courses_datetimes(courses=courses, tz_courses=tz_courses)
    period_courses = courses.loc[(courses["courses_datetimes"] < max_date) & 
                                 (courses["courses_datetimes"] > min_date)]
    if verbosity:
        print("Number of finished courses inside the given period: %d" %len(period_courses))
    return period_courses.reset_index(drop=True)



def load_traces_dates(traces_file):
    """@brief Returns the datetimes of the traces defined in the given file.

    @:param traces_file Path to the traces file.
    @return A list of traces datetimes. Thoses datetimes do not have a timezone information.
    """
    traces_dates = []
    with open(traces_file, 'rb') as f:
        for i, line in enumerate(f):
            d = json.loads(line)
            try:
                date = datetime.datetime.strptime(d["created_at"], '%Y-%m-%dT%H:%M:%S.%fZ')
            except ValueError:
                date = datetime.datetime.strptime(d["created_at"], '%Y-%m-%dT%H:%M:%SZ')
            traces_dates.append(int(date.timestamp()))
    return traces_dates



def get_drivers_traces(traces_file, driver_ids, tz_traces, verbosity=1):
    """@brief Returns a dataframe of traces corresponding to the given list of drivers.
    Might take time as the traces_file is parsed by this method.

    @:param traces_file Path to the traces file.
    @:param driver_ids List of drivers_ids
    @:param tz_traces The timezone of the traces.
    @:param verbosity Increase level of verbosity. Default to 1.
    @return A dataframe containing the subset of traces corresponding to the given drivers.
    """
    driver_traces = []
    try:
        with open(traces_file, 'rb') as f:
            for i, line in enumerate(f):
                d = json.loads(line)
                if str(d["id"]) in driver_ids:
                    driver_traces.append(d)
    except Exception as e:
        raise Exception(("Encountered an error while reading the traces file %s" 
                         "\n%s"%(traces_file,str(e))))    
    driver_traces_df = pd.DataFrame(driver_traces)
    if "created_at" not in driver_traces_df.columns:
        raise Exception("The traces have no datetimes : missing the \"created_at\" attribute.")    
    driver_traces_df["id"] = driver_traces_df["id"].astype(str)
    driver_traces_df["created_at"] = pd.to_datetime(driver_traces_df["created_at"])
    driver_traces_df["created_at"] = driver_traces_df["created_at"].dt.tz_localize(tz_traces, ambiguous='NaT')
    if verbosity:
        print("We obtain %d traces corresponding to the given drivers ids." %len(driver_traces_df))
    return driver_traces_df
    


def get_courses_dataset(courses_file, tz_courses, min_duration, max_duration, areas, verbosity=1):
    """@brief Returns a dataset of courses from the given file with start datetimes 
    in the given timezone.

    @:param courses_file Path to the courses file.
    @:param tz_courses The timezone of the courses.
    @:param verbosity Increase level of verbosity. Default to 1.
    @return A courses dataframe.
    """
    courses = pd.read_csv(courses_file, sep=";")
    courses = hc.keep_finished_courses(courses, verbosity=verbosity)
    courses = hc.keep_car_courses(courses, verbosity=verbosity)
    courses = hc.get_courses_in_areas(courses=courses, areas=areas, verbosity=verbosity)
    courses = hc.keep_duration_courses(lb=min_duration, ub=max_duration, 
                                    courses=courses, verbosity=verbosity)
    courses = hc.remove_loop_courses(courses=courses, verbosity=verbosity)

    courses = hc.set_courses_lat_lng(courses)
    courses = set_courses_datetimes(courses=courses, tz_courses=tz_courses)
    
#     courses["driver_id"] = pd.to_numeric(courses["driver_id"]) # Cast driver ids to integers

    if verbosity:
        print("Number of finished courses : %d" %len(courses))
    return courses



def get_driver_traces(driver_id, traces, verbosity=1):
    """@brief Returns the subset of traces corresponding to the given driver.

    @:param driver_id Id of the driver we are interested in.
    @:param traces Dataframe of traces.
    @:param verbosity Increase level of verbosity. Default to 1.
    @return A traces dataframe corresponding to the given driver, sorted by date.
    """
    if traces.empty:
        return traces
    required_columns = {"id", "created_at"}
    if not required_columns.issubset(traces.columns):
        raise TypeError(("The given traces dataframe is missing at least one of the following "
                         "columns : \"id\" and \"created_at\"."))

    driver_traces = traces.loc[traces["id"] == driver_id]
    driver_traces = driver_traces.sort_values(by='created_at').reset_index(drop=True)
    if verbosity:
        print("We obtain %d traces for the driver %s" %(len(driver_traces), driver_id))
    return driver_traces



def get_time_window_traces(lb, ub, traces):
    """@brief Returns the subset of traces that occured in the given timewindow.

    @:param lb Lower bound of the timewindow. Must be a datetime.
    @:param ub Upper bound of the timewindow. Must be a datetime.
    @:param traces Dataframe of traces.
    @:param verbosity Increase level of verbosity. Default to 1.
    @return A traces dataframe that occured in the given timewindow.
    """
    if "created_at" not in traces.columns:
        raise TypeError(("The given traces dataframe is missing the \"created_at\" column."))
    return traces.loc[(traces["created_at"] >= lb) & (traces["created_at"] < ub)]



def get_stop_trace(lat, lng, traces, max_dist_to_pos, first=True):
    """@brief Returns the trace closest to the given position. 
    The distance between the two must be inferior to the given radius max_dist_to_pos.
    If none is found, returns -1.

    @:param lat The latitude of the position of interest.
    @:param lng The longitude of the position of interest.
    @:param traces Dataframe of traces.
    @:param max_dist_to_pos Radius under which we consider the trace valid.
    @:param first Boolean indicating if we want the first closest position or the last.
    @return The closest trace as an object.
    """
    traces["dist_to_stop"] = utils.haversine(lat_1=lat, lng_1=lng, 
                                             lat_2=traces["lat"], lng_2=traces["lng"])
    traces = traces.sort_values(by="created_at", ascending=first)
    stop_trace = -1
    if traces["dist_to_stop"].min() < max_dist_to_pos :
        stop_trace = traces["dist_to_stop"].idxmin() 
    traces = traces.drop(labels="dist_to_stop", axis=1)
    return stop_trace



def filter_positions(positions, std_deviation):
    dist = utils.haversine(np.array(positions["lat"])[1:], np.array(positions["lng"])[1:],
                           np.array(positions["lat"])[:-1], np.array(positions["lng"])[:-1])
    positions["dist_to_next"] = np.append(dist,np.inf)
    positions = positions.loc[positions["dist_to_next"] > 2*std_deviation]
    return positions.drop("dist_to_next", axis=1)



def get_course_traces(course, traces, TW_radius, max_dist_to_pos, std_deviation, verbosity=1):
    """@brief Returns the subset of traces corresponding to the given course.
    Firstly, the trace corresponding to the origin will be searched around the 
    start time of the course. The trace corresponding to the destination will 
    be then searched in the following trace up to 100 minutes after the 
    supposed start datetime of the course.

    @:param course The course of interest.
    @:param traces Dataframe of traces.
    @:param TW_radius The radius of the time-window around the start datetime of 
                     the course used to search the origin trace. Indeed, the 
                     former will be search in the TW :
                         [course_datetime - TW_radius, course_datetime + TW_radius]
    @:param max_dist_to_pos Max distance under at which the closest trace is 
                           considered valid. Used for finding the origin and 
                           destination traces.
    @:param verbosity Increase level of verbosity. Default to 1.
    @return A dataframe of traces representing the driver trajectory during the 
            course sorted by datetime.
    """
    if traces.empty:
        return traces
    
    required_columns = {"courses_datetimes", "driver_id", "request_id", "lat_origin", "lon_origin", "lat_destination", "lon_destination"}
    if not required_columns.issubset(course.keys()):
        missing_features = set(required_columns) - set(course.keys())
        missing_features = [str(f) for f in missing_features]
        raise Exception("The given course is missing the following attribute(s) : '%s'" 
                        %(', '.join(missing_features)))

    driver_traces = get_driver_traces(driver_id=course["driver_id"], traces=traces, verbosity=verbosity)
    if driver_traces.empty:
        return pd.DataFrame()

    # Find the origin trace
    origin_trace_TW_lb = course["courses_datetimes"] - datetime.timedelta(minutes=TW_radius)
    origin_trace_TW_ub = course["courses_datetimes"] + datetime.timedelta(minutes=TW_radius)
    origin_TW_traces = get_time_window_traces(lb=origin_trace_TW_lb, ub=origin_trace_TW_ub, 
                                              traces=driver_traces)
    if origin_TW_traces.empty:
        return pd.DataFrame()
    origin_pos = get_stop_trace(lat=course["lat_origin"], lng=course["lon_origin"], 
                                traces=origin_TW_traces, max_dist_to_pos=max_dist_to_pos, 
                                first=False)
    if origin_pos == -1:
        return pd.DataFrame()

    # Find the destination trace
    dest_traces = driver_traces.loc[origin_pos:]
    dest_trace_TW_lb = course["courses_datetimes"] - datetime.timedelta(minutes=TW_radius)
    dest_trace_TW_ub = course["courses_datetimes"] + datetime.timedelta(minutes=100)
    dest_TW_traces = get_time_window_traces(lb=dest_trace_TW_lb, ub=dest_trace_TW_ub, 
                                            traces=dest_traces)
    if dest_TW_traces.empty:
        return pd.DataFrame()
    destination_pos = get_stop_trace(lat=course["lat_destination"], lng=course["lon_destination"], 
                                     traces=dest_TW_traces, max_dist_to_pos=max_dist_to_pos, first=True)
    if destination_pos == -1:
        return pd.DataFrame()

    # Compute the course traces
    course_traces = driver_traces.loc[origin_pos:destination_pos]
    course_traces["course_id"] = [course['request_id']] * len(course_traces)
    course_traces["created_at"] = pd.to_datetime(course_traces["created_at"])
    course_traces["created_at"] = course_traces["created_at"].apply(lambda x : int(x.timestamp()))
    
    if verbosity:
        print("Course %d : found %d traces." %(course["request_id"], len(course_traces)))

    return course_traces



def write_traces(traces, traces_output_file, verbosity=1):
    """@brief Writes the given traces in the given file.

    @:param traces Dataframe of traces to output.
    @:param traces_output_file Path to the output file.
    @:param verbosity Increase level of verbosity. Default to 1.
    """
    utils.create_directory(directory=os.path.dirname(traces_output_file), verbosity=verbosity)
    traces.to_csv(traces_output_file, sep=",", index=False)
    if verbosity:
        print("Outputed the traces in the file " + traces_output_file)



def write_positions(positions, positions_output_file, verbosity=1):
    """@brief Writes the given positions (lat,lng) in the given csv file.
    Format used to plot the positions on a map.

    @:param positions Dataframe of positions to output.
    @:param positions_output_file Path to the output file.
    @:param verbosity Increase level of verbosity. Default to 1.
    """
    required_columns = {'lat','lng'}
    if not required_columns.issubset(positions.columns):
        raise Exception("The given positions are missing at least one of the following column(s) : \"lat\", \"lng\"")

    utils.create_directory(directory=os.path.dirname(positions_output_file), verbosity=verbosity)
    positions["marker"] = ["circle1"]*len(positions)
    positions["color"] = ["red"]*len(positions)
    positions["legend"] = range(len(positions))
    positions[['lat','lng','marker','color','legend']].to_csv(positions_output_file, sep="\t", index=False)
    if verbosity:
        print("Outputed the positions in the file " + positions_output_file)



def get_course_real_duration(course_traces):
    """@brief Returns the course duration based on its traces.

    @:param course_traces Dataframe of the coure traces.
    """
    if course_traces.empty:
        return 0
    if "created_at" not in course_traces.columns:
        raise Exception("The given traces are missing the attribute \"created_at\"")
    return course_traces.iloc[-1]["created_at"] - course_traces.iloc[0]["created_at"]



def link_traces_to_courses(traces_file, courses_file, 
                           courses_dataset_output_file, output_traces_dir,
                           area_params_file, saasoffice_timezone, std_deviation,
                           TW_radius, max_dist_to_pos, min_duration=5, max_duration=90, verbosity=1):
    """@brief Links the given traces dataset to the given courses.

    @:param courses_file Dataframe of courses.
    @:param traces_file Dataframe of traces.
    @:param saasoffice_timezone Timezone of the courses saasoffice.
    @:param TW_radius The radius of the time-window around the start datetime of 
                     the course used to search the origin trace. Indeed, the 
                     former will be search in the TW :
                       [course_datetime - TW_radius, course_datetime + TW_radius]
    @:param max_dist_to_pos Max distance under at which the closest trace is 
                           considered valid. Used for finding the origin and 
                           destination traces.
    @:param verbosity Increase level of verbosity. Default to 1.
    """
    # time zones
    tz_courses = pytz.timezone(saasoffice_timezone)
    tz_traces = pytz.UTC

    ## COURSES DATASET
    graph = g.Graph(vertices=pd.DataFrame(), edges=pd.DataFrame(), params=area_params_file, verbosity=verbosity)
    courses = get_courses_dataset(courses_file=courses_file, tz_courses=tz_courses, min_duration=min_duration, max_duration=max_duration, areas=graph.area_clusters, verbosity=verbosity)
    courses = courses.reset_index(drop=True)

    ## Get the courses corresponding to the drivers
    drivers_traces = pd.read_csv(traces_file, sep=";")
    drivers_traces["created_at"] = pd.to_datetime(drivers_traces["created_at"], utc=True)
    drivers_traces["id"] = drivers_traces["id"].apply(str)
    drivers_traces = drivers_traces.sort_values("created_at")

    max_trace_date = drivers_traces["created_at"].max()
    min_trace_date = drivers_traces["created_at"].min()
    period_courses = get_courses_in_period(courses=courses, max_date=max_trace_date, min_date=min_trace_date,
                                            verbosity=verbosity)
    period_courses.to_csv(courses_dataset_output_file, index=False, sep=";")
    print("Number of courses in period %f to %f : %d" %(min_trace_date.timestamp(), max_trace_date.timestamp(), len(period_courses)))

    ## Get the traces corresponding to the drivers
    period_courses_ids = list(period_courses["driver_id"].unique())
    period_drivers_traces = drivers_traces.loc[drivers_traces["id"].isin(period_courses_ids)]

    ## No traffic courses 
    no_traffic_period = hc.Period(0,4,0,4)
    no_traffic_courses = hc.get_period_courses(courses=courses, period=no_traffic_period, 
                                               verbosity=verbosity)

    period_courses["real_duration"] = [0]*len(period_courses)
    nb_courses = 0
    for i, course in period_courses.iterrows():
        if verbosity:
            print(">>>> COURSE %d" %(nb_courses))
        course_traces = get_course_traces(course=course, traces=period_drivers_traces, 
                                            TW_radius=TW_radius, max_dist_to_pos=max_dist_to_pos, 
                                            std_deviation=std_deviation, verbosity=verbosity)
        
        if verbosity:
            print("     Found %d traces for course %d." %(len(course_traces), i))
        if len(course_traces) < 10:
            continue

        period_courses.loc[i,"real_duration"] = get_course_real_duration(course_traces)

        nb_traces_before_filtering = len(course_traces)
        course_traces = filter_positions(positions=course_traces, std_deviation=std_deviation)
        if verbosity:
            print("Course %d : found %d traces and %d after filtering." %(course["request_id"], nb_traces_before_filtering, len(course_traces)))

        # Output trace if no traffic course
        #if i in no_traffic_courses.index:   
        traces_output_file = output_traces_dir + "/traces_course_" + str(nb_courses) + ".csv"
        write_traces(traces=course_traces, traces_output_file=traces_output_file, verbosity=verbosity)
        positions_output_file = output_traces_dir + "/pos_traces_course_" + str(nb_courses) + ".csv"
        write_positions(positions=course_traces, positions_output_file=positions_output_file, verbosity=verbosity)
        nb_courses += 1

    return courses

