from unittest import TestCase
import os
import logging
import lib.utils.utils as utils


class TestUtils(TestCase):
    def test_create_directory(self):
        """
        Test create_directory function.
        """
        log = logging.getLogger("trace_clustering")
        log.setLevel(logging.NOTSET)

        directory = "./test" 
        utils.create_directory(directory=directory, log=log)
        self.assertTrue( os.path.isdir(directory) )
        os.rmdir(directory)
        self.assertTrue( not os.path.isdir(directory) )

        directory = "./test/test2" 
        utils.create_directory(directory=directory, log=log)
        self.assertTrue( os.path.isdir("./test") )
        self.assertTrue( os.path.isdir(directory) )
        os.rmdir(directory)
        os.rmdir("./test")
        self.assertTrue( not os.path.isdir("./test") )
        self.assertTrue( not os.path.isdir(directory) )