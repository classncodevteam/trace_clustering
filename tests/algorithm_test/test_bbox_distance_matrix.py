from unittest import TestCase
import os
import logging
import numpy as np
import lib.algorithm.bbox_distance_matrix as dist
import lib.models.bbox as bb

class TestBBoxDistanceMatrix(TestCase):
    def test_get_bbox_distances(self):
        """
        Test get_bbox_distances function.
        """
        log = logging.getLogger("trace_clustering")
        log.setLevel(logging.NOTSET)

        bboxes = [bb.BBox(lat_max=48.65468584817256, lat_min=48.39273786659243, lng_max=1.95556640625, lng_min=1.5216064453125),
                  bb.BBox(lat_max=48.32432226398979, lat_min=48.03496000186445, lng_max=2.6977764454535347, lng_min=2.1814190235785347),
                  bb.BBox(lat_max=48.84395433115633, lat_min=48.28047369392051, lng_max=2.1264873829535347, lng_min=1.2475811329535347),
                  bb.BBox(lat_max=48.49481829932261, lat_min=48.14414906924925, lng_max=3.0108867970160347, lng_min=2.4121319142035347),
                  bb.BBox(lat_max=48.513016295775614, lat_min=48.29055420543425, lng_max=2.997153886859784, lng_min=2.6428448048285347)]
        bbox_distance_matrix = dist.BBoxDistanceMatrix()
        bbox_distances = bbox_distance_matrix.get_bbox_distance_matrix(bboxes=bboxes, parallel=True, log=log)
        expected_distances = np.array([
            [0, 64397.81857, 0, 75300.99458, 80880.68534],
            [64397.81857, 0, 69998.74369, 48494.05125, 1550775.15960],
            [0, 69998.74369, 0, 80245.77122, 85389.75312],
            [75300.99458, 48494.05125, 80245.77122, 0, 1081.45625],
            [80880.68534, 1550775.15960, 85389.75312, 1081.45625, 0]])
        self.assertTrue( (abs(expected_distances - bbox_distances) < np.full(shape=(5,5), fill_value=0.00001)).all() )

        bboxes = [bb.BBox(lat_max=11, lat_min=9, lng_max=4, lng_min=2, id=1),
                  bb.BBox(lat_max=10, lat_min=8, lng_max=3, lng_min=1, id=2),
                  bb.BBox(lat_max=12, lat_min=7, lng_max=5, lng_min=0, id=3),
                  bb.BBox(lat_max=8, lat_min=6, lng_max=6, lng_min=4, id=4),
                  bb.BBox(lat_max=3, lat_min=1, lng_max=11, lng_min=9, id=5)]
        bbox_distances = bbox_distance_matrix.get_bbox_distance_matrix(bboxes=bboxes, parallel=True, log=log)
        expected_distances = np.array([
            [0, 467573.25911883, 0, 399554.28650979, 1178784.53936435],
            [467573.25911883, 0, 0, 398206.54864906, 1178508.87263636],
            [0, 0, 0, 1174941.71922767, 1176001.8783965],
            [399554.28650979, 398206.54864906, 1174941.71922767, 0, 784928.61806476],
            [1178784.53936435, 1178508.87263636, 1176001.8783965, 784928.61806476, 0]])
        self.assertTrue( (abs(expected_distances - bbox_distances) < np.full(shape=(5,5), fill_value=0.00001)).all() )

        # Empty bbooxes array
        bboxes = np.array([])
        bbox_distances = bbox_distance_matrix.get_bbox_distance_matrix(bboxes=bboxes, parallel=True, log=log)
        self.assertTrue( ( bbox_distances == np.array([]) ).all() )
