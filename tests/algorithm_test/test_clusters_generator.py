from unittest import TestCase

import lib.models.ride as rd
import lib.algorithm.clusters_generator as cg


class TestClustersGenerator(TestCase):
    def test_generate(self):
        # Rides
        rides = [rd.Ride(id=0, driver_id=1, lat_max=11, lat_min=9, lng_max=4, lng_min=2),
                 rd.Ride(id=1, driver_id=1, lat_max=10, lat_min=8, lng_max=3, lng_min=1),
                 rd.Ride(id=2, driver_id=1, lat_max=12, lat_min=7, lng_max=5, lng_min=0),
                 rd.Ride(id=3, driver_id=1, lat_max=8, lat_min=6, lng_max=6, lng_min=4),
                 rd.Ride(id=4, driver_id=1, lat_max=3, lat_min=1, lng_max=21, lng_min=19),
                 rd.Ride(id=5, driver_id=1, lat_max=4, lat_min=2, lng_max=22, lng_min=20),
                 rd.Ride(id=6, driver_id=1, lat_max=3, lat_min=1, lng_max=23, lng_min=21),
                 rd.Ride(id=7, driver_id=1, lat_max=2, lat_min=0, lng_max=22, lng_min=20),
                 rd.Ride(id=8, driver_id=1, lat_max=20, lat_min=19, lng_max=23, lng_min=22),
                 rd.Ride(id=9, driver_id=1, lat_max=20, lat_min=18, lng_max=24, lng_min=22),
                 rd.Ride(id=10, driver_id=1, lat_max=21, lat_min=18, lng_max=24, lng_min=21),
                 rd.Ride(id=11, driver_id=1, lat_max=19, lat_min=17, lng_max=25, lng_min=23)]
        labels = [0,0,0,0,1,1,1,1,2,2,2,2]
        clusters = cg.ClustersGenerator.generate(bbox_containers=rides, labels=labels)
        self.assertTrue(len(clusters) == 3)
        self.assertTrue(clusters[0].bbox.lat_max == 12 and
                        clusters[0].bbox.lat_min == 6 and 
                        clusters[0].bbox.lng_max == 6 and
                        clusters[0].bbox.lng_min == 0)
        self.assertTrue(clusters[1].bbox.lat_max == 4 and
                        clusters[1].bbox.lat_min == 0 and 
                        clusters[1].bbox.lng_max == 23 and
                        clusters[1].bbox.lng_min == 19)
        self.assertTrue(clusters[2].bbox.lat_max == 21 and
                        clusters[2].bbox.lat_min == 17 and 
                        clusters[2].bbox.lng_max == 25 and
                        clusters[2].bbox.lng_min == 21)

        
        labels = [0,0,0,0,1,1,1,1,2,2,0,1]
        clusters = cg.ClustersGenerator.generate(bbox_containers=rides, labels=labels)
        self.assertTrue(len(clusters) == 3)
        self.assertTrue(clusters[0].bbox.lat_max == 21 and
                        clusters[0].bbox.lat_min == 6 and 
                        clusters[0].bbox.lng_max == 24 and
                        clusters[0].bbox.lng_min == 0)
        self.assertTrue(clusters[1].bbox.lat_max == 19 and
                        clusters[1].bbox.lat_min == 0 and 
                        clusters[1].bbox.lng_max == 25 and
                        clusters[1].bbox.lng_min == 19)
        self.assertTrue(clusters[2].bbox.lat_max == 20 and
                        clusters[2].bbox.lat_min == 18 and 
                        clusters[2].bbox.lng_max == 24 and
                        clusters[2].bbox.lng_min == 22)
