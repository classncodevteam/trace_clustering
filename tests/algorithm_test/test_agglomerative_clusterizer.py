from unittest import TestCase
import numpy as np

import lib.algorithm.agglomerative_clusterizer as ac

class TestAgglomerativeClusterizer(TestCase):
    def test_fit(self):
        matrix_distance = np.array([
            [0,11,12,15],
            [11,0,1,4],
            [12,1,0,3],
            [15,4,3,0]
        ])

        # Distance threshold
        agglomerative_clusterizer = ac.AgglomerativeClusterizer(10, 1000)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 1, 1, 1]).all() )

        agglomerative_clusterizer = ac.AgglomerativeClusterizer(1, 1000)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 1, 2, 3]).all() )

        agglomerative_clusterizer = ac.AgglomerativeClusterizer(2, 1000)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 1, 1, 2]).all() )

        agglomerative_clusterizer = ac.AgglomerativeClusterizer(16, 1000)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 0, 0, 0]).all() )

        # Cluster size
        agglomerative_clusterizer = ac.AgglomerativeClusterizer(1000, 3)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 1, 1, 1]).all() )

        agglomerative_clusterizer = ac.AgglomerativeClusterizer(1000, 1)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 1, 2, 3]).all() )

        agglomerative_clusterizer = ac.AgglomerativeClusterizer(1000, 2)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 1, 1, 2]).all() )

        agglomerative_clusterizer = ac.AgglomerativeClusterizer(1000, 4)
        labels = agglomerative_clusterizer.fit(matrix_distance=matrix_distance)
        self.assertTrue( (labels == [0, 0, 0, 0]).all() )
