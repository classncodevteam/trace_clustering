from unittest import TestCase
import lib.models.bbox as bb
import lib.algorithm.bbox_njit_utils as bbox_njit_utils
import warnings
warnings.filterwarnings("ignore")

class TestBBoxNjitUtils(TestCase):
    def test_haversine(self):
        """
        Test haversine function.
        """
        self.assertTrue( bbox_njit_utils.haversine(0,0,0,0) == 0 )
        self.assertTrue( bbox_njit_utils.haversine(1,2,1,2) == 0 )

        self.assertTrue(abs( bbox_njit_utils.haversine(48.884297, 2.339264, 48.886251, 2.342107) - 300.70011) < 0.0001)
        self.assertTrue(abs( bbox_njit_utils.haversine(48.884297, 2.339264, 48.821070, 2.287322) - 7991.94379) < 0.0001)
        self.assertTrue(abs( bbox_njit_utils.haversine(-48.884297, -2.339264, 48.821070, -2.287322) - 10864342.26016) < 0.0001)


    def test_to_radians(self):
        """
        Test to_radians function.
        """
        self.assertTrue(bbox_njit_utils.to_radians(0) == 0)
        self.assertTrue(abs( bbox_njit_utils.to_radians(90) - 1.5707963267948966 ) < 0.001)
        self.assertTrue(abs( bbox_njit_utils.to_radians(180) - 3.141592653589793 ) < 0.001)
        self.assertTrue(abs( bbox_njit_utils.to_radians(45) - 0.7853981633974483 ) < 0.001)

    
    def test_bbox_area(self):
        """
        Test get_bbox_area function.
        """
        bbox = bb.BBox(lat_max=1, lat_min=1, lng_max=0, lng_min=0, id=0)
        self.assertTrue(bbox_njit_utils.get_bbox_area(bbox=bbox.get_coordinates()) == 0)

        bbox = bb.BBox(lat_max=41.00361532382567, lat_min=36.99255058126725, lng_max=-102.05228801565558, lng_min=-109.01762004690558, id=0)
        self.assertTrue( abs(bbox_njit_utils.get_bbox_area(bbox=bbox.get_coordinates()) - 269010722473.12653) < 0.000001 )

        bbox = bb.BBox(lat_max=48.889288, lat_min=48.842888, lng_max=2.383461, lng_min=2.298819, id=0)
        self.assertTrue( abs(bbox_njit_utils.get_bbox_area(bbox=bbox.get_coordinates()) - 32015046.58223184) < 0.000001 )


    def test_get_bboxes_intersection(self):
        """
        Test get_bboxes_intersection function.
        """
        ## FRANCE
        bbox1 = bb.BBox(lat_max=48.65468584817256, lat_min=48.39273786659243, lng_max=1.95556640625, lng_min=1.5216064453125)
        bbox2 = bb.BBox(lat_max=48.32432226398979, lat_min=48.03496000186445, lng_max=2.6977764454535347, lng_min=2.1814190235785347)
        bbox3 = bb.BBox(lat_max=48.84395433115633, lat_min=48.28047369392051, lng_max=2.1264873829535347, lng_min=1.2475811329535347)
        bbox4 = bb.BBox(lat_max=48.49481829932261, lat_min=48.14414906924925, lng_max=3.0108867970160347, lng_min=2.4121319142035347)
        bbox5 = bb.BBox(lat_max=48.513016295775614, lat_min=48.29055420543425, lng_max=2.997153886859784, lng_min=2.6428448048285347)
        # No intersection 1 et 2
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == 0 )

        # Inclusion 1 et 3
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( abs( intersection_area - 932970899.5805811) < 0.001 )
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox3.get_coordinates(), bbox2=bbox1.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( abs( intersection_area - 932970899.5805811) < 0.001 )
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox3.get_coordinates(), bbox2=bbox3.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( abs( intersection_area - 4061562985.192755) < 0.001 )

        # Intersection 2 et 4
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox2.get_coordinates(), bbox2=bbox4.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( abs( intersection_area - 424804624.2825143) < 0.001 )
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox4.get_coordinates(), bbox2=bbox2.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( abs( intersection_area - 424804624.2825143) < 0.001 )
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox2.get_coordinates(), bbox2=bbox5.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( abs( intersection_area - 15289026.99603270) < 0.001 )

        ## ALGER
        bbox1 = bb.BBox(lat_max=33.375848266560155, lat_min=33.21055131800179, lng_max=9.59280283579813, lng_min=9.39779551157938)
        bbox2 = bb.BBox(lat_max=33.23582528042144, lat_min=33.084072054160266, lng_max=9.71639902720438, lng_min=9.55709726939188)
        bbox3 = bb.BBox(lat_max=33.58774863840576, lat_min=33.37814185157729, lng_max=9.35934336314188, lng_min=9.165709329938755)
        bbox4 = bb.BBox(lat_max=33.652999027016236, lat_min=33.31281795738083, lng_max=9.424445940349155, lng_min=9.09897596964603)

        # No intersection 1 et 3
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == 0 )

        # Inclusion 3 et 4
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox3.get_coordinates(), bbox2=bbox4.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == bbox3.get_area() )
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox4.get_coordinates(), bbox2=bbox3.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == bbox3.get_area() )
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox4.get_coordinates(), bbox2=bbox4.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == bbox4.get_area() )

        # Intersections
        # 1 et 2
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( (intersection_area - 9354913.219104776) < 0.001 )

        # 1 et 4
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox4.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( (intersection_area - 17389268.271956086) < 0.001 )

        ## USA
        bbox1 = bb.BBox(lat_max=41.881852252320710, lat_min=41.380951433027270, lng_max=-113.7421250990197, lng_min=-114.3573594740197)
        bbox2 = bb.BBox(lat_max=41.446865043454864, lat_min=41.025520537635146, lng_max=-113.2916856458947, lng_min=-113.8080430677697)
        bbox3 = bb.BBox(lat_max=40.837719102993425, lat_min=40.470988684613780, lng_max=-113.6083127039699, lng_min=-114.1905880945949)
        bbox4 = bb.BBox(lat_max=42.007477370539966, lat_min=41.355173581826975, lng_max=-113.6907101649074, lng_min=-114.6080685633449)

        # No intersection 1 et 3
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == 0 )

        # Inclusion 1 et 4
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox4.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == bbox1.get_area() )
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox4.get_coordinates(), bbox2=bbox1.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( intersection_area == bbox1.get_area() )

        # Intersections
        # 1 et 2
        intersection = bbox_njit_utils.get_bboxes_intersection(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates())
        intersection_area = bbox_njit_utils.get_bbox_area(intersection)
        self.assertTrue( ( intersection_area - 40378848.00319065 ) < 0.001 )


    def test_get_intersection_distance_between_bboxes(self):
        """
        Test get_intersection_distance_between_bboxes function.
        """
        ## FRANCE
        bbox1 = bb.BBox(lat_max=48.65468584817256, lat_min=48.39273786659243, lng_max=1.95556640625, lng_min=1.5216064453125)
        bbox2 = bb.BBox(lat_max=48.32432226398979, lat_min=48.03496000186445, lng_max=2.6977764454535347, lng_min=2.1814190235785347)
        bbox3 = bb.BBox(lat_max=48.84395433115633, lat_min=48.28047369392051, lng_max=2.1264873829535347, lng_min=1.2475811329535347)
        bbox4 = bb.BBox(lat_max=48.49481829932261, lat_min=48.14414906924925, lng_max=3.0108867970160347, lng_min=2.4121319142035347)
        bbox5 = bb.BBox(lat_max=48.513016295775614, lat_min=48.29055420543425, lng_max=2.997153886859784, lng_min=2.6428448048285347)
        # No intersection 1 et 2
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates()) == 1 )

        # Inclusion 1 et 3
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates()) == 0 )
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox3.get_coordinates(), bbox2=bbox1.get_coordinates()) == 0 )
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox3.get_coordinates(), bbox2=bbox3.get_coordinates()) == 0 )

        # Intersection 2 et 4
        self.assertTrue( (bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox2.get_coordinates(), bbox2=bbox4.get_coordinates()) - 1.906289572336882) < 0.001 )
        self.assertTrue( (bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox2.get_coordinates(), bbox2=bbox5.get_coordinates()) - 41.413603679765224) < 0.001 )

        ## ALGER
        bbox1 = bb.BBox(lat_max=33.375848266560155, lat_min=33.21055131800179, lng_max=9.59280283579813, lng_min=9.39779551157938)
        bbox2 = bb.BBox(lat_max=33.23582528042144, lat_min=33.084072054160266, lng_max=9.71639902720438, lng_min=9.55709726939188)
        bbox3 = bb.BBox(lat_max=33.58774863840576, lat_min=33.37814185157729, lng_max=9.35934336314188, lng_min=9.165709329938755)
        bbox4 = bb.BBox(lat_max=33.652999027016236, lat_min=33.31281795738083, lng_max=9.424445940349155, lng_min=9.09897596964603)

        # No intersection 1 et 3
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates()) == 1 )

        # Inclusion 3 et 4
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox3.get_coordinates(), bbox2=bbox4.get_coordinates()) == 0 )
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox4.get_coordinates(), bbox2=bbox3.get_coordinates()) == 0 )
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox4.get_coordinates(), bbox2=bbox4.get_coordinates()) == 0 )

        # Intersections
        # 1 et 2
        self.assertTrue( (bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates()) - 25.807893608999716) < 0.001 )

        # 1 et 4
        self.assertTrue( (bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox4.get_coordinates()) - 18.200687069472362) < 0.001 )

        ## USA
        bbox1 = bb.BBox(lat_max=41.881852252320710, lat_min=41.380951433027270, lng_max=-113.7421250990197, lng_min=-114.3573594740197)
        bbox2 = bb.BBox(lat_max=41.446865043454864, lat_min=41.025520537635146, lng_max=-113.2916856458947, lng_min=-113.8080430677697)
        bbox3 = bb.BBox(lat_max=40.837719102993425, lat_min=40.470988684613780, lng_max=-113.6083127039699, lng_min=-114.1905880945949)
        bbox4 = bb.BBox(lat_max=42.007477370539966, lat_min=41.355173581826975, lng_max=-113.6907101649074, lng_min=-114.6080685633449)

        # No intersection 1 et 3
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates()) == 1 )

        # Inclusion 1 et4
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox4.get_coordinates()) == 0 )
        self.assertTrue( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox4.get_coordinates(), bbox2=bbox1.get_coordinates()) == 0 )

        # Intersections
        # 1 et 2
        self.assertTrue( ( bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates()) - 49.21024653844973 ) < 0.001 )