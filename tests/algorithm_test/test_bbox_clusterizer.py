from unittest import TestCase
import os
import logging
import numpy as np
import lib.models.ride as rd
import lib.models.bbox_container as bbc
import lib.algorithm.bbox_clusterizer as bc

class TestBBoxClusterizer(TestCase):
    def test_fit(self):
        log = logging.getLogger("trace_clustering")
        log.setLevel(logging.NOTSET)

        rides = [rd.Ride(id=0, driver_id=1, lat_max=11, lat_min=9, lng_max=4, lng_min=2),
                rd.Ride(id=1, driver_id=1, lat_max=10, lat_min=8, lng_max=3, lng_min=1),
                rd.Ride(id=2, driver_id=1, lat_max=12, lat_min=7, lng_max=5, lng_min=0),
                rd.Ride(id=3, driver_id=1, lat_max=8, lat_min=6, lng_max=6, lng_min=4),
                rd.Ride(id=4, driver_id=1, lat_max=3, lat_min=1, lng_max=21, lng_min=19),
                rd.Ride(id=5, driver_id=1, lat_max=4, lat_min=2, lng_max=22, lng_min=20),
                rd.Ride(id=6, driver_id=1, lat_max=3, lat_min=1, lng_max=23, lng_min=21),
                rd.Ride(id=7, driver_id=1, lat_max=2, lat_min=0, lng_max=22, lng_min=20),
                rd.Ride(id=8, driver_id=1, lat_max=20, lat_min=19, lng_max=23, lng_min=22),
                rd.Ride(id=9, driver_id=1, lat_max=20, lat_min=18, lng_max=24, lng_min=22),
                rd.Ride(id=10, driver_id=1, lat_max=21, lat_min=18, lng_max=24, lng_min=21),
                rd.Ride(id=11, driver_id=1, lat_max=19, lat_min=17, lng_max=25, lng_min=23)]

        bbox_clusterizer = bc.BboxClusterizer(distance_threshold=1000000, size_cluster_max=5)
        clusters = bbox_clusterizer.fit(bbox_containers=rides, log=log)

        cluster_0 = [bbox.id for bbox in clusters[0].bbox_containers]
        self.assertTrue( cluster_0 == [0,1,2,3] )

        cluster_1 = [bbox.id for bbox in clusters[1].bbox_containers]
        self.assertTrue( cluster_1 == [4, 5, 6, 7] )

        cluster_2 = [bbox.id for bbox in clusters[2].bbox_containers]
        self.assertTrue( cluster_2 == [8, 9, 10, 11] )


        rides = []
        clusters = bbox_clusterizer.fit(bbox_containers=rides, log=log)
        self.assertTrue( len(clusters) == 0 )
