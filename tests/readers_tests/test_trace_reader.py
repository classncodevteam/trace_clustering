from unittest import TestCase
import logging
import lib.readers.rides_reader as rr


class TestRidesReader(TestCase):
    def test_read(self):
        """
        Test parse_traces_files function.
        """
        log = logging.getLogger("trace_clustering")
        log.setLevel(logging.NOTSET)

        # Normal execution
        rides_file = "./tests/data_test/test_rides.csv"
        rides_reader = rr.RidesReader(file=rides_file, log=log)
        rides = rides_reader.read(log=log)

        self.assertTrue( rides[0].driver_id == 36703 )
        self.assertTrue( abs(rides[0].bbox.lat_max - 39.483800) < 0.001 )
        self.assertTrue( abs(rides[0].bbox.lat_min - 39.376600) < 0.001 )
        self.assertTrue( abs(rides[0].bbox.lng_max - -6.345870) < 0.001 )
        self.assertTrue( abs(rides[0].bbox.lng_min - -6.379590) < 0.001 )

        self.assertTrue( rides[1].driver_id == 36703 )
        self.assertTrue( abs(rides[1].bbox.lat_max - 39.469100) < 0.001 )
        self.assertTrue( abs(rides[1].bbox.lat_min - 39.469000) < 0.001 )
        self.assertTrue( abs(rides[1].bbox.lng_max - -6.380150) < 0.001 )
        self.assertTrue( abs(rides[1].bbox.lng_min - -6.380170) < 0.001 )

        self.assertTrue( rides[2].driver_id == 36703 )
        self.assertTrue( abs(rides[2].bbox.lat_max - 39.469500) < 0.001 )
        self.assertTrue( abs(rides[2].bbox.lat_min - 39.467100) < 0.001 )
        self.assertTrue( abs(rides[2].bbox.lng_max - -6.381330) < 0.001 )
        self.assertTrue( abs(rides[2].bbox.lng_min - -6.382570) < 0.001 )

        self.assertTrue( rides[3].driver_id == 36703 )
        self.assertTrue( abs(rides[3].bbox.lat_max - 39.474600) < 0.001 )
        self.assertTrue( abs(rides[3].bbox.lat_min - 39.467200) < 0.001 )
        self.assertTrue( abs(rides[3].bbox.lng_max - -6.385030) < 0.001 )
        self.assertTrue( abs(rides[3].bbox.lng_min - -6.392840) < 0.001 )

        self.assertTrue( rides[4].driver_id == 36703 )
        self.assertTrue( abs(rides[4].bbox.lat_max - 39.483300) < 0.001 )
        self.assertTrue( abs(rides[4].bbox.lat_min - 39.475900) < 0.001 )
        self.assertTrue( abs(rides[4].bbox.lng_max - -6.381590) < 0.001 )
        self.assertTrue( abs(rides[4].bbox.lng_min - -6.390970) < 0.001 )

        # File does not exist
        null_file = "pouet.csv"
        try:
            rides_reader = rr.RidesReader(file=null_file, log=log)
        except Exception as e:
            self.assertTrue(str(e) == ("The rides file " + null_file + " does not exist."))

