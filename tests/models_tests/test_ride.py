from unittest import TestCase
import pandas as pd
import numpy as np
import lib.models.ride as rd
import lib.models.driver as drvr



class TestRide(TestCase):
    def test_ride(self):
        """
        Test ride function.
        """
        driver = drvr.Driver(id=21)
        
        ride1 = rd.Ride(id=1, driver_id=21, lat_max=48.878000, lat_min=48.866300, lng_max=2.413490, lng_min=2.367120)
        self.assertTrue(np.array_equal(ride1.bbox.get_coordinates(), [48.878000, 48.866300, 2.413490, 2.367120]))

        ride2 = rd.Ride(id=2, driver_id=21, lat_max=48.884400, lat_min=48.871900, lng_max=2.370680, lng_min=2.344890)
        self.assertTrue(np.array_equal(ride2.bbox.get_coordinates(), [48.884400, 48.871900, 2.370680, 2.344890]))

        ride3 = rd.Ride(id=3, driver_id=23, lat_max=48.891200, lat_min=48.884600, lng_max=2.349680, lng_min=2.345840)
        self.assertTrue(np.array_equal(ride3.bbox.get_coordinates(), [48.891200, 48.884600, 2.349680, 2.345840]))
