from unittest import TestCase
import lib.models.cluster as c
import lib.models.bbox_container as bc
import lib.models.bbox as bb


class TestCluster(TestCase):
    def test_add_bbox_container(self):
        """
        Test add_bbox_container function.
        """
        cluster = c.Cluster(id=0)

        bbox = bb.BBox(lat_max=1, lat_min=-1, lng_max=1, lng_min=-1)
        bbox_container = bc.BBoxContainer(bbox=bbox, id=0)
        cluster.add_bbox_container(bbox_container=bbox_container)

        self.assertTrue( len( cluster.bbox_containers ) == 1 )

        self.assertTrue( cluster.bbox.lat_max == 1 )
        self.assertTrue( cluster.bbox.lat_min == -1 )
        self.assertTrue( cluster.bbox.lng_max == 1 )
        self.assertTrue( cluster.bbox.lng_min == -1 )

        bbox = bb.BBox(lat_max=2, lat_min=-1, lng_max=1, lng_min=-1)
        bbox_container = bc.BBoxContainer(bbox=bbox, id=0)
        cluster.add_bbox_container(bbox_container=bbox_container)

        self.assertTrue( len( cluster.bbox_containers ) == 2 )
        self.assertTrue( cluster.bbox.lat_max == 2 )
        self.assertTrue( cluster.bbox.lat_min == -1 )
        self.assertTrue( cluster.bbox.lng_max == 1 )
        self.assertTrue( cluster.bbox.lng_min == -1 )

        bbox = bb.BBox(lat_max=6, lat_min=4, lng_max=6, lng_min=4)
        bbox_container = bc.BBoxContainer(bbox=bbox, id=0)
        cluster.add_bbox_container(bbox_container=bbox_container)

        self.assertTrue( len( cluster.bbox_containers ) == 3 )
        self.assertTrue( cluster.bbox.lat_max == 6 )
        self.assertTrue( cluster.bbox.lat_min == -1 )
        self.assertTrue( cluster.bbox.lng_max == 6 )
        self.assertTrue( cluster.bbox.lng_min == -1 )