from unittest import TestCase
import os
import pandas as pd
import numpy as np
import lib.models.driver as drvr
import lib.models.ride as rd


class TestDriver(TestCase):
    def test_add_ride(self):
        """
        Test add_ride function.
        """
        driver = drvr.Driver(id=21)
        
        ride1 = rd.Ride(id=1, driver_id=21, lat_max=48.878000, lat_min=48.866300, lng_max=2.413490, lng_min=2.367120)
        driver.add_ride(ride=ride1)
        self.assertTrue(np.array_equal(driver.bbox.get_coordinates(), [48.878, 48.8663, 2.41349, 2.36712]))

        ride2 = rd.Ride(id=2, driver_id=21, lat_max=48.884400, lat_min=48.871900, lng_max=2.370680, lng_min=2.344890)
        driver.add_ride(ride=ride2)
        self.assertTrue(np.array_equal(driver.bbox.get_coordinates(), [48.8844, 48.8663, 2.41349, 2.34489]))

        ride3 = rd.Ride(id=3, driver_id=23, lat_max=48.891200, lat_min=48.884600, lng_max=2.349680, lng_min=2.345840)
        try:
            driver.add_ride(ride=ride3)
        except Exception as e :
            self.assertTrue(str(e) == "The given ride belongs to the driver 23 and not to the driver 21.")
