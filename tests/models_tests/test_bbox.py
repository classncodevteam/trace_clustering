from unittest import TestCase
import numpy as np
import lib.models.bbox as bb


class TestBBox(TestCase):
    def test_constructor(self):
        """
        Test to_radians function.
        """
        bbox = bb.BBox()
        self.assertTrue( bbox.lat_max == 0.0 and
                         bbox.lat_min == 0.0 and
                         bbox.lng_max == 0.0 and
                         bbox.lng_min == 0.0 and
                         bbox.id() == 0)

        bbox = bb.BBox(lat_max=50, lat_min=49, lng_max=3, lng_min=2, id=1)
        self.assertTrue( bbox.lat_max == 50.0 and
                         bbox.lat_min == 49.0 and
                         bbox.lng_max == 3.0 and
                         bbox.lng_min == 2.0 and
                         bbox.id() == 1)

        try:
            bbox = bb.BBox(lat_max=49, lat_min=50, lng_max=3, lng_min=2, id=1)
            self.assertTrue( bbox.lat_max == 50.0 and
                             bbox.lat_min == 49.0 and
                             bbox.lng_max == 3.0 and
                             bbox.lng_min == 2.0 and
                             bbox.id() == 1)
        except Exception as e:
            self.assertTrue(str(e) == "The given area is not valid : (lat_max = %.3f, lat_min = %.3f, lng_max = %.3f, lng_min = %.3f)"
                  %(49, 50, 3, 2))

        try:
            bbox = bb.BBox(lat_max=50, lat_min=49, lng_max=2, lng_min=3, id=1)
            self.assertTrue( bbox.lat_max == 50.0 and
                             bbox.lat_min == 49.0 and
                             bbox.lng_max == 3.0 and
                             bbox.lng_min == 2.0 and
                             bbox.id() == 1)
        except Exception as e:
            self.assertTrue(str(e) == "The given area is not valid : (lat_max = %.3f, lat_min = %.3f, lng_max = %.3f, lng_min = %.3f)"
                  %(50, 49, 2, 3))


    def test_get_coordinates(self):
        """
        Test get_coordinates function.
        """
        bbox = bb.BBox()
        self.assertTrue( (bbox.get_coordinates() == np.array([0.0, 0.0, 0.0, 0.0])).all())

        bbox = bb.BBox(lat_max=50, lat_min=49, lng_max=3, lng_min=2, id=1)
        self.assertTrue((bbox.get_coordinates() == np.array([50,49,3,2])).all())


    def test_area(self):
        """
        Test test_area function.
        """
        bbox = bb.BBox(lat_max=1, lat_min=1, lng_max=0, lng_min=0, id=0)
        self.assertTrue(bbox.get_area() == 0)

        bbox = bb.BBox(lat_max=41.00361532382567, lat_min=36.99255058126725, lng_max=-102.05228801565558, lng_min=-109.01762004690558, id=0)
        self.assertTrue( (bbox.get_area() - 269010722473.12653) < 0.000001 )

        bbox = bb.BBox(lat_max=48.889288, lat_min=48.842888, lng_max=2.383461, lng_min=2.298819, id=0)
        self.assertTrue( (bbox.get_area() - 32015046.58223184) < 0.000001 )


    def test_intersection(self):
        """
        Test intersection function.
        """
        ## FRANCE
        bbox1 = bb.BBox(lat_max=48.65468584817256, lat_min=48.39273786659243, lng_max=1.95556640625, lng_min=1.5216064453125)
        bbox2 = bb.BBox(lat_max=48.32432226398979, lat_min=48.03496000186445, lng_max=2.6977764454535347, lng_min=2.1814190235785347)
        bbox3 = bb.BBox(lat_max=48.84395433115633, lat_min=48.28047369392051, lng_max=2.1264873829535347, lng_min=1.2475811329535347)
        bbox4 = bb.BBox(lat_max=48.49481829932261, lat_min=48.14414906924925, lng_max=3.0108867970160347, lng_min=2.4121319142035347)
        bbox5 = bb.BBox(lat_max=48.513016295775614, lat_min=48.29055420543425, lng_max=2.997153886859784, lng_min=2.6428448048285347)
        # No intersection 1 et 2
        self.assertTrue( bbox1.intersection(bbox2).get_area() == 0 )

        # Inclusion 1 et 3
        self.assertTrue( abs( bbox1.intersection(bbox3).get_area() - 932970899.5805811) < 0.001 )
        self.assertTrue( abs( bbox3.intersection(bbox1).get_area() - 932970899.5805811) < 0.001 )
        self.assertTrue( abs( bbox3.intersection(bbox3).get_area() - 4061562985.192755) < 0.001 )

        # Intersection 2 et 4
        self.assertTrue( abs(bbox2.intersection(bbox4).get_area() - 424804624.2825143) < 0.001 )
        self.assertTrue( abs(bbox4.intersection(bbox2).get_area() - 424804624.2825143) < 0.001 )
        self.assertTrue( abs(bbox2.intersection(bbox5).get_area() - 15289026.99603270) < 0.001 )

        ## ALGER
        bbox1 = bb.BBox(lat_max=33.375848266560155, lat_min=33.21055131800179, lng_max=9.59280283579813, lng_min=9.39779551157938)
        bbox2 = bb.BBox(lat_max=33.23582528042144, lat_min=33.084072054160266, lng_max=9.71639902720438, lng_min=9.55709726939188)
        bbox3 = bb.BBox(lat_max=33.58774863840576, lat_min=33.37814185157729, lng_max=9.35934336314188, lng_min=9.165709329938755)
        bbox4 = bb.BBox(lat_max=33.652999027016236, lat_min=33.31281795738083, lng_max=9.424445940349155, lng_min=9.09897596964603)

        # No intersection 1 et 3
        self.assertTrue( bbox1.intersection(bbox3).get_area() == 0 )

        # Inclusion 3 et 4
        self.assertTrue( bbox3.intersection(bbox4).get_area() == bbox3.get_area() )
        self.assertTrue( bbox4.intersection(bbox3).get_area() == bbox3.get_area() )
        self.assertTrue( bbox4.intersection(bbox4).get_area() == bbox4.get_area() )

        # Intersections
        # 1 et 2
        self.assertTrue( abs(bbox1.intersection(bbox2).get_area() - 9354913.219104776) < 0.001 )

        # 1 et 4
        self.assertTrue( abs(bbox1.intersection(bbox4).get_area() - 17389268.271956086) < 0.001 )

        ## USA
        bbox1 = bb.BBox(lat_max=41.881852252320710, lat_min=41.380951433027270, lng_max=-113.7421250990197, lng_min=-114.3573594740197)
        bbox2 = bb.BBox(lat_max=41.446865043454864, lat_min=41.025520537635146, lng_max=-113.2916856458947, lng_min=-113.8080430677697)
        bbox3 = bb.BBox(lat_max=40.837719102993425, lat_min=40.470988684613780, lng_max=-113.6083127039699, lng_min=-114.1905880945949)
        bbox4 = bb.BBox(lat_max=42.007477370539966, lat_min=41.355173581826975, lng_max=-113.6907101649074, lng_min=-114.6080685633449)

        # No intersection 1 et 3
        self.assertTrue( bbox1.intersection(bbox3).get_area() == 0 )

        # Inclusion 1 et 4
        self.assertTrue( bbox1.intersection(bbox4).get_area() == bbox1.get_area() )
        self.assertTrue( bbox4.intersection(bbox1).get_area() == bbox1.get_area() )

        # Intersections
        # 1 et 2
        self.assertTrue( abs( bbox1.intersection(bbox2).get_area() - 40378848.00319065 ) < 0.001 )


    def test_distance_to(self):
        """
        Test distance_to function.
        """
        ## FRANCE
        bbox1 = bb.BBox(lat_max=48.65468584817256, lat_min=48.39273786659243, lng_max=1.95556640625, lng_min=1.5216064453125)
        bbox2 = bb.BBox(lat_max=48.32432226398979, lat_min=48.03496000186445, lng_max=2.6977764454535347, lng_min=2.1814190235785347)
        bbox3 = bb.BBox(lat_max=48.84395433115633, lat_min=48.28047369392051, lng_max=2.1264873829535347, lng_min=1.2475811329535347)
        bbox4 = bb.BBox(lat_max=48.49481829932261, lat_min=48.14414906924925, lng_max=3.0108867970160347, lng_min=2.4121319142035347)
        bbox5 = bb.BBox(lat_max=48.513016295775614, lat_min=48.29055420543425, lng_max=2.997153886859784, lng_min=2.6428448048285347)
        # No intersection 1 et 2
        self.assertTrue( (bbox1.distance_to(bbox2=bbox2) - 64397.81857) < 0.001 )

        # Inclusion 1 et 3
        self.assertTrue( bbox1.distance_to(bbox2=bbox3) == 0 )
        self.assertTrue( bbox3.distance_to(bbox2=bbox1) == 0 )
        self.assertTrue( bbox3.distance_to(bbox2=bbox3) == 0 )

        # Intersection 2 et 4
        self.assertTrue( (bbox2.distance_to(bbox2=bbox5) - 1550775.159595) < 0.001 )
        self.assertTrue( (bbox2.distance_to(bbox2=bbox4) - 48494.051254) < 0.001 )

        ## ALGER
        bbox1 = bb.BBox(lat_max=33.375848266560155, lat_min=33.21055131800179, lng_max=9.59280283579813, lng_min=9.39779551157938)
        bbox2 = bb.BBox(lat_max=33.23582528042144, lat_min=33.084072054160266, lng_max=9.71639902720438, lng_min=9.55709726939188)
        bbox3 = bb.BBox(lat_max=33.58774863840576, lat_min=33.37814185157729, lng_max=9.35934336314188, lng_min=9.165709329938755)
        bbox4 = bb.BBox(lat_max=33.652999027016236, lat_min=33.31281795738083, lng_max=9.424445940349155, lng_min=9.09897596964603)

        # No intersection 1 et 3
        self.assertTrue( ( bbox1.distance_to(bbox2=bbox3) - 30202.825881) < 0.001 )

        # Inclusion 3 et 4
        self.assertTrue( bbox3.distance_to(bbox2=bbox4) == 0 )
        self.assertTrue( bbox4.distance_to(bbox2=bbox3) == 0 )
        self.assertTrue( bbox4.distance_to(bbox2=bbox4) == 0 )

        # Intersections
        # 1 et 2
        self.assertTrue( (bbox1.distance_to(bbox2=bbox2) - 511390.299377) < 0.001)

        # 1 et 4
        self.assertTrue( (bbox1.distance_to(bbox2=bbox4) - 550647.137937) < 0.001 )

        ## USA
        bbox1 = bb.BBox(lat_max=41.881852252320710, lat_min=41.380951433027270, lng_max=-113.7421250990197, lng_min=-114.3573594740197)
        bbox2 = bb.BBox(lat_max=41.446865043454864, lat_min=41.025520537635146, lng_max=-113.2916856458947, lng_min=-113.8080430677697)
        bbox3 = bb.BBox(lat_max=40.837719102993425, lat_min=40.470988684613780, lng_max=-113.6083127039699, lng_min=-114.1905880945949)
        bbox4 = bb.BBox(lat_max=42.007477370539966, lat_min=41.355173581826975, lng_max=-113.6907101649074, lng_min=-114.6080685633449)

        # No intersection 1 et 3
        self.assertTrue( (bbox1.distance_to(bbox2=bbox3) - 109369.212117) < 0.001)

        # Inclusion 1 et 4
        self.assertTrue( bbox1.distance_to(bbox2=bbox4) == 0 )
        self.assertTrue( bbox4.distance_to(bbox2=bbox1) == 0 )

        # Intersections
        # 1 et 2
        self.assertTrue( (bbox1.distance_to(bbox2=bbox2) - 2980266.7297166777) < 0.001 )


    def test_empty(self):
        """
        Test empty function.
        """
        bbox = bb.BBox(lat_max=0, lat_min=0, lng_max=0, lng_min=0)
        self.assertTrue(bbox.empty())

        bbox = bb.BBox(lat_max=10, lat_min=10, lng_max=10, lng_min=10)
        self.assertTrue(bbox.empty())

        bbox = bb.BBox(lat_max=15, lat_min=10, lng_max=10, lng_min=10)
        self.assertTrue(bbox.empty())

        bbox = bb.BBox(lat_max=10, lat_min=10, lng_max=15, lng_min=10)
        self.assertTrue(bbox.empty())

        bbox = bb.BBox(lat_max=15, lat_min=10, lng_max=15, lng_min=10)
        self.assertTrue(not bbox.empty())


    def test_distance_between_coordinate_bboxes(self):
        """
        Test distance_between_bboxes_coordinates function.
        """
        ## FRANCE
        bbox1 = bb.BBox(lat_max=48.65468584817256, lat_min=48.39273786659243, lng_max=1.95556640625, lng_min=1.5216064453125)
        bbox2 = bb.BBox(lat_max=48.32432226398979, lat_min=48.03496000186445, lng_max=2.6977764454535347, lng_min=2.1814190235785347)
        bbox3 = bb.BBox(lat_max=48.84395433115633, lat_min=48.28047369392051, lng_max=2.1264873829535347, lng_min=1.2475811329535347)
        bbox4 = bb.BBox(lat_max=48.49481829932261, lat_min=48.14414906924925, lng_max=3.0108867970160347, lng_min=2.4121319142035347)
        bbox5 = bb.BBox(lat_max=48.513016295775614, lat_min=48.29055420543425, lng_max=2.997153886859784, lng_min=2.6428448048285347)
        # No intersection 1 et 2
        self.assertTrue( (bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates()) - 64397.81857) < 0.001 )

        # Inclusion 1 et 3
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates()) == 0 )
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox3.get_coordinates(), bbox2=bbox1.get_coordinates()) == 0 )
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox3.get_coordinates(), bbox2=bbox3.get_coordinates()) == 0 )

        # Intersection 2 et 4
        self.assertTrue( (bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox2.get_coordinates(), bbox2=bbox4.get_coordinates()) - 48494.051254) < 0.001 )
        self.assertTrue( (bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox2.get_coordinates(), bbox2=bbox5.get_coordinates()) - 1550775.159595) < 0.001 )

        ## ALGER
        bbox1 = bb.BBox(lat_max=33.375848266560155, lat_min=33.21055131800179, lng_max=9.59280283579813, lng_min=9.39779551157938)
        bbox2 = bb.BBox(lat_max=33.23582528042144, lat_min=33.084072054160266, lng_max=9.71639902720438, lng_min=9.55709726939188)
        bbox3 = bb.BBox(lat_max=33.58774863840576, lat_min=33.37814185157729, lng_max=9.35934336314188, lng_min=9.165709329938755)
        bbox4 = bb.BBox(lat_max=33.652999027016236, lat_min=33.31281795738083, lng_max=9.424445940349155, lng_min=9.09897596964603)

        # No intersection 1 et 3
        self.assertTrue( ( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates()) - 30202.825881) < 0.001 )

        # Inclusion 3 et 4
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox3.get_coordinates(), bbox2=bbox4.get_coordinates()) == 0 )
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox4.get_coordinates(), bbox2=bbox3.get_coordinates()) == 0 )
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox4.get_coordinates(), bbox2=bbox4.get_coordinates()) == 0 )

        # Intersections
        # 1 et 2
        self.assertTrue( (bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates()) - 511390.299377) < 0.001)

        # 1 et 4
        self.assertTrue( (bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox4.get_coordinates()) - 550647.137937) < 0.001 )

        ## USA
        bbox1 = bb.BBox(lat_max=41.881852252320710, lat_min=41.380951433027270, lng_max=-113.7421250990197, lng_min=-114.3573594740197)
        bbox2 = bb.BBox(lat_max=41.446865043454864, lat_min=41.025520537635146, lng_max=-113.2916856458947, lng_min=-113.8080430677697)
        bbox3 = bb.BBox(lat_max=40.837719102993425, lat_min=40.470988684613780, lng_max=-113.6083127039699, lng_min=-114.1905880945949)
        bbox4 = bb.BBox(lat_max=42.007477370539966, lat_min=41.355173581826975, lng_max=-113.6907101649074, lng_min=-114.6080685633449)

        # No intersection 1 et 3
        self.assertTrue( (bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox3.get_coordinates()) - 109369.212117) < 0.001)

        # Inclusion 1 et 4
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox4.get_coordinates()) == 0 )
        self.assertTrue( bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox4.get_coordinates(), bbox2=bbox1.get_coordinates()) == 0 )

        # Intersections
        # 1 et 2
        self.assertTrue( (bb.BBox.distance_between_bboxes_coordinates(bbox1=bbox1.get_coordinates(), bbox2=bbox2.get_coordinates()) - 2980266.7297166777) < 0.001 )


    def test_merge_bboxes(self):
        """
        Test merge_bboxes function.
        """
        bbox1 = bb.BBox(lat_max=3, lat_min=0, lng_max=3, lng_min=0, id=1)
        bbox2 = bb.BBox(lat_max=6, lat_min=3, lng_max=6, lng_min=3, id=1)
        merged_bboxes = bb.BBox.merge_bboxes(bbox1 = bbox1, bbox2 = bbox2)
        self.assertTrue(merged_bboxes.lat_max == 6 and 
                        merged_bboxes.lat_min == 0 and 
                        merged_bboxes.lng_max == 6 and 
                        merged_bboxes.lng_min == 0 and 
                        merged_bboxes.id() == 1)

        bbox1 = bb.BBox(lat_max=3, lat_min=0, lng_max=3, lng_min=0, id=2)
        bbox2 = bb.BBox(lat_max=5, lat_min=-4, lng_max=6, lng_min=3, id=2)
        merged_bboxes = bb.BBox.merge_bboxes(bbox1 = bbox1, bbox2 = bbox2)
        self.assertTrue(merged_bboxes.lat_max == 5 and 
                        merged_bboxes.lat_min == -4 and 
                        merged_bboxes.lng_max == 6 and 
                        merged_bboxes.lng_min == 0 and 
                        merged_bboxes.id() == 2)

        # Not the same id
        bbox1 = bb.BBox(lat_max=3, lat_min=0, lng_max=3, lng_min=0, id=1)
        bbox2 = bb.BBox(lat_max=5, lat_min=-4, lng_max=6, lng_min=3, id=2)
        merged_bboxes = bb.BBox.merge_bboxes(bbox1 = bbox1, bbox2 = bbox2)
        self.assertTrue(merged_bboxes == bbox1)

        # One of the bboxes is None
        bbox1 = bb.BBox(lat_max=3, lat_min=0, lng_max=3, lng_min=0, id=1)
        bbox2 = None
        merged_bboxes = bb.BBox.merge_bboxes(bbox1 = bbox1, bbox2 = bbox2)
        self.assertTrue(merged_bboxes == bbox1)

        bbox1 = None
        bbox2 = bb.BBox(lat_max=6, lat_min=0, lng_max=6, lng_min=0, id=2)
        merged_bboxes = bb.BBox.merge_bboxes(bbox1 = bbox1, bbox2 = bbox2)
        self.assertTrue(merged_bboxes == bbox2)