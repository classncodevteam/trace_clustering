from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='trace_clustering',
      version='1.1.0',
      description='Clusterize a set of rides into sub area. Useful to then map-match those courses to the subareas.',
      long_description=readme(),
      url='https://class-n-co.atlassian.net/wiki/spaces/TECH/pages/473989208/Driver+Trace+Clustering',
      author='Yuso',
      package_dir={'trace_clustering': 'lib'},
      packages=['trace_clustering'],
      install_requires=[
          'numpy==1.19.0',
          'pandas==1.0.5',
          'sklearn==0.0',
          'numba==0.50.1',
          'plotly==4.9.0',
          'argparse',
          'nose==1.3.7',
          'psycopg2-binary==2.8.5'
      ],
      test_suite='nose.collector',
      tests_require=['nose'],
      zip_safe=False)
