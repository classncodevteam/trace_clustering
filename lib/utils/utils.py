import os
import logging
import numpy as np
from numba import njit

def create_directory(directory, log: logging.Logger):
    """
    @brief Recursive function that creates the directory if it does not exist.

    @param directory Path to the directory we want to create.
    @param log Logger to output information.
    """
    if not directory or os.path.exists(directory):
        return

    parent_dir = os.path.dirname(directory)
    if not os.path.isdir(parent_dir):
        create_directory(directory=parent_dir, log=log)

    log.debug("Creating directory " + directory)
    os.mkdir(directory)