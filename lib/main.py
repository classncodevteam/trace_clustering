import time, random
import logging
import argparse

import algorithm.rides_clusterizer as rc
from db import RideSql
from datetime import datetime, date, timedelta

logging.getLogger('numba').setLevel(logging.CRITICAL)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=('Geographical clustering of a set of courses into subareas.'))

    # INPUT
    parser.add_argument("-d", "--date", type=str, nargs='?', default='daily', help=("Maximum number of rides per generated cluster."))
    parser.add_argument("-s", "--size_cluster_max", type=int, nargs='?', default=150, help=("Maximum number of rides per generated cluster."))
    parser.add_argument("-t", "--distance_threshold", type=float, nargs='?', default=10000, help=("Path to the directory in which can be found the map matched courses. All the "
                                                                                "csv and txt files in this directory will be taken into account.")) # with courses
    parser.add_argument("-v", "--verbosity", help="Output verbosity.")

    parser.add_argument('--all', dest='with_clusterized_rides', action='store_true')
    parser.add_argument('--only-not-clusterized', dest='with_clusterized_rides', action='store_false')
    parser.set_defaults(with_clusterized_rides=False)


    args = parser.parse_args()
    
    if(args.date == 'daily'):
        args.date = str(date.today() - timedelta(days=1))
    date = datetime.strptime(args.date, '%Y-%m-%d')

        
    # LOGGING
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger()

    start_time = time.time()
    log.info("READ FILES")

    rides = RideSql.get_rides_for_day(date, args.with_clusterized_rides)
    log.info("rides: %d" %len(rides))
    log.info("--- read %d rides in %s seconds ---" % (len(rides), (time.time() - start_time)))

    start_time = time.time()
    log.info("CLUSTERING")
    rides_clusterizer = rc.RidesClusterizer(distance_threshold = args.distance_threshold, size_cluster_max = args.size_cluster_max)
    clusters = rides_clusterizer.clusterize(rides=rides, log=log)
    log.info("--- clusterize %d clusters in %s seconds ---" % (len(clusters), (time.time() - start_time)))

    for cluster in clusters:
        cluster.set_cluster_id_on_rides()