from typing import List

import models.cluster as c
import models.bbox_container as bc

class ClustersGenerator:
    """
    @brief Object that generates clusters from the given bbox_containers and labels.
    """
    @staticmethod
    def generate(bbox_containers: List[bc.BBoxContainer], labels: List[float], first_cluster_id: int = 0) -> List[c.Cluster]:
        """
        Generates a vector of Clusters from the given bbox_containers and labels.

        @:param bbox_containers: A list of bbox containers.
        @:param labels: A list of labels.
        @:param first_cluster_id: Id of the first generated cluster.
        """
        if len(bbox_containers) == 0:
            return []

        if len(bbox_containers) == 1:
            cluster = c.Cluster(id=first_cluster_id)
            cluster.add_bbox_container(bbox_containers[0])
            return [cluster]

        if len(bbox_containers) != len(labels):
            raise Exception("The length of given bbox container list (%d) does not match the length of the given labels (%d)." %(len(bbox_containers), len(labels)))
        
        sorted_unique_labels = list(set(labels))
        sorted_unique_labels.sort()

        clusters = []
        for label in sorted_unique_labels:
            clusters.append(c.Cluster(id=first_cluster_id + label))
        for (bbox_container, label) in zip(bbox_containers, labels):
            cluster = clusters[int(label)]
            cluster.add_bbox_container(bbox_container=bbox_container)

        return clusters
