import time
import logging
import sys
from typing import List

import algorithm.bbox_distance_matrix as dm
import algorithm.agglomerative_clusterizer as ac
import algorithm.clusters_generator as cg
import models.cluster as cl
import models.bbox_container as bc

class BboxClusterizer:
    """
    @brief Class that clusters a given set of bboxes.
    """

    def __init__(self, distance_threshold: float, size_cluster_max: int) -> None:
        """
        @brief Constructs a new BboxClusterizer object with the given parameters.

        @:param distance_threshold The distance threshold between 2 clusters.
        @:param size_cluster_max The maximal number of elements per cluster.
        """
        self.distance_threshold = distance_threshold
        self.size_cluster_max = size_cluster_max


    def fit(self, bbox_containers: List[bc.BBoxContainer], log: logging.Logger, first_cluster_id: int = 0, parallel: bool = True) -> List[cl.Cluster]:
        """
        @brief Clusters the given BBox containers.

        @:param bbox_containers List of bbox containers that we want ot clusterize.
        @:param first_cluster_id Id of the first cluster. Default to 0.
        @:param parallel Boolean indicating if the distance computing must be parallelized.
        @:param log Logger to output information.
        """
        log.info(">> Clustering traces")

        bboxes = [bbox_container.bbox for bbox_container in bbox_containers]

        if len(bboxes) == 0 or len(bboxes) == 1:
            return cg.ClustersGenerator.generate(bbox_containers=bbox_containers, labels=[], first_cluster_id=first_cluster_id)

        # Bbox distances
        start_time = time.time()
        bboxes_distances = dm.BBoxDistanceMatrix.get_bbox_distance_matrix(bboxes, parallel=parallel, log=log)
        log.debug("--- get_bbox_distances : %s seconds ---" % (time.time() - start_time))

        # Clustering
        start_time = time.time()
        agglomerative_clusterizer = ac.AgglomerativeClusterizer(
            distance_threshold=self.distance_threshold,
            size_cluster_max=self.size_cluster_max)
        labels = agglomerative_clusterizer.fit(matrix_distance=bboxes_distances)
        log.debug("--- hierarchical_clustering : %s seconds ---" % (time.time() - start_time))

        # Generate clusters
        start_time = time.time()
        clusters = cg.ClustersGenerator.generate(bbox_containers=bbox_containers, labels=labels, first_cluster_id=first_cluster_id)
        log.debug("--- set_clusters : %s seconds ---" % (time.time() - start_time))

        return clusters
