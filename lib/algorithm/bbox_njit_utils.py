import math
import numpy as np
from numba import njit
import utils.utils as utils

R = 6378137.0
DEGREES_TO_RADIANS = math.pi / 180.

@njit
def haversine(lat1, lng1, lat2, lng2):
    """
    @brief Compute the Haversine distance .

    @:param lat1: Latitude of the first point.
    @:param lng1: Longitude of the first point.
    @:param lat2: Latitude of the second point.
    @:param lng2: Longitude of the second point.
    """
    R = 6371.0
    TO_RAD = 0.01745329251994

    lng1Rad = lng2 * TO_RAD
    lat1Rad = lat2 * TO_RAD
    lng2Rad = lng1 * TO_RAD
    lat2Rad = lat1 * TO_RAD

    havLat = np.power( np.sin( (lat2Rad - lat1Rad) / 2 ), 2)
    havLng = np.power( np.sin( (lng2Rad - lng1Rad) / 2 ), 2)
    return (2 * R * np.arcsin( np.sqrt( havLat + np.cos(lat1Rad) * np.cos(lat2Rad) * havLng ) )) * 1000

@njit
def to_radians(degree):
    """
    @brief Convert degrees to radians.

    @:param degree: The angle in degrees.
    """
    return degree * DEGREES_TO_RADIANS


@njit
def get_bbox_area(bbox):
    """
    @brief Computes the area of the bbox.

    @:param bbox: The area for which we want to compute the bbox as a np.array.
    """
    area = 0
    if bbox[0] == bbox[1] or bbox[2] == bbox[3]:
        return float(area)
    coordinates = [(bbox[0], bbox[3]),
                   (bbox[0], bbox[2]),
                   (bbox[1], bbox[2]),
                   (bbox[1], bbox[3]),
                   (bbox[0], bbox[3])]
    if len(coordinates) > 2 :
        for i in range(len(coordinates)-1):
            p1 = coordinates[i]
            p2 = coordinates[i + 1]
            area += to_radians(p2[1] - p1[1]) * (2.0 + math.sin(to_radians(p1[0])) + math.sin(to_radians(p2[0])))
        area = (area * R * R) / 2.0
    return math.fabs(area)


@njit
def get_bboxes_intersection(bbox1, bbox2):
    """
    @brief Computes the intersection between two BBoxes.

    @:param bbox1 The first bbox coordinates as a np.array.
    @:param bbox2 The second bbox coordinates as a np.array.
    """
    intersection_lat_min = max(bbox1[1], bbox2[1])
    intersection_lat_max = min(bbox1[0], bbox2[0])
    intersection_lng_min = max(bbox1[3], bbox2[3])
    intersection_lng_max = min(bbox1[2], bbox2[2])
    if intersection_lat_min >= intersection_lat_max or intersection_lng_min >= intersection_lng_max:
        return [0., 0., 0., 0.]
    return [float(intersection_lat_max),
            float(intersection_lat_min),
            float(intersection_lng_max),
            float(intersection_lng_min)]

@njit
def get_intersection_distance_between_bboxes(bbox1, bbox2):
    """
    @brief Computes the distance between two BBoxes.

    @:param bbox1 The first BBox coordinates as a np.array.
    @:param bbox2 The second BBox coordinates as a np.array.
    """
    intersection = get_bboxes_intersection(bbox1,bbox2)
    if get_bbox_area(intersection) == 0:
        return 1
    return ((min(get_bbox_area(bbox1), get_bbox_area(bbox2)) / get_bbox_area(intersection)) - 1)
