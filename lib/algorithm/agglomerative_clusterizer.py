import time
import os
import numpy as np
from sklearn.cluster import AgglomerativeClustering

import utils.utils as utils
import models.bbox as bb


class AgglomerativeClusterizer:
    """
    @brief Agglomerative Clustering of a distance matrix. Recursively merges the pair of clusters 
    that minimally increases a given linkage distance.
    """

    def __init__(self, distance_threshold: float, size_cluster_max: int):
        """
        @brief Constructs a new TraceClusterizer object with the given parameters.

        @:param distance_threshold The distance threshold between 2 clusters.
        @:param size_cluster_max The maximal number of elements per cluster.
        """
        self.distance_threshold = distance_threshold
        self.size_cluster_max = size_cluster_max


    def fit(self, matrix_distance: np.array) -> np.array:
        """
        Clustering (takes also into account the size of the clusters, the distance, etc), 
        returns a vector of labels corresponding to the bbox labels in the given matrix.
        
        @:param matrix_distance The given distance matrix on which the clustering will be based.
        """
        model = AgglomerativeClustering(distance_threshold = self.distance_threshold,
                                        n_clusters = None,
                                        compute_full_tree = True,
                                        affinity = 'precomputed',
                                        linkage = 'single')
        model.fit(matrix_distance)
        return self.cut_tree__(model = model)


    def cut_tree__(self, model: AgglomerativeClustering) -> np.array:
        """
        Cuts the clustering tree obtained in the given AgglomerativeClustering model in order to 
        check the nb of elements per cluster and the distance between each element in the same 
        cluster. Returns a vector of labels.

        :param model: The fitted AgglomerativeClustering model.
        """
        n = len(model.distances_) + 1
        for k in range(n):
            if k >= len(model.distances_):
                break
            if model.distances_[k] >= self.distance_threshold:
                break
        return self.cut_tree_nb_clusters__(model = model, nb_clusters = (n - k))


    def cut_tree_nb_clusters__(self, model: AgglomerativeClustering, nb_clusters: int) -> np.array:
        """
        Cuts the clustering tree obtained in the given AgglomerativeClustering model in order to 
        check the nb of elements per cluster. Returns a vector of labels.
        
        :param model: The fitted AgglomerativeClustering model.
        :param nb_clusters: The max number of elements per cluster.
        """
        n = len(model.children_) + 1

        last_merge = -np.ones(n, dtype=int)
        for k in range(n - nb_clusters):
            m1 = int(model.children_[k][0])
            m2 = int(model.children_[k][1])

            if (m1 < n and m2 < n): # both single observables
                if 2 > self.size_cluster_max:
                    continue
                last_merge[m1] = k
                last_merge[m2] = k
            elif (m1 < n or m2 < n): # one is a cluster
                if (m1 < n):
                    j = m1
                    m1 = m2 - n # the cluster
                else :
                    j = m2

                # do not merge if next cluster is larger than threashold
                size_next_cluster = np.count_nonzero(last_merge == m1) + 1
                if size_next_cluster > self.size_cluster_max:
                    continue

                # merging single observable and cluster
                for l in range(n):
                    if (last_merge[l] == m1):
                        last_merge[l] = k # belonged to the cluster
                last_merge[j] = k # the one that is not a cluster
            else : # both clusters
                # do not merge if next cluster is larger than threshold
                size_next_cluster = np.count_nonzero(last_merge == m1 - n) + np.count_nonzero(last_merge == m2 - n)
                if size_next_cluster > self.size_cluster_max:
                    continue

                for l in range(n):
                    if (last_merge[l] == m1 - n) or (last_merge[l] == m2 - n) :
                        last_merge[l] = k

        labels = -np.ones(n)
        label = -1
        z = -np.ones(n)
        for j in range(n):
            if last_merge[j] == -1: # still singleton
                label += 1
                labels[j] = label
            else:
                if z[last_merge[j]] < 0:
                    label += 1
                    z[last_merge[j]] = label
                labels[j] = z[last_merge[j]]

        return labels
