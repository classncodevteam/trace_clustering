import logging
from typing import List
import numpy as np
import numba as nb

import models.bbox as bb


class BBoxDistanceMatrix:
    """
    @brief Class used to compute a distance matrix between BBoxes.
    """
    @staticmethod
    def get_bbox_distance_matrix(bboxes: List[bb.BBox], log: logging.Logger, parallel: bool = True) -> np.array:
        """
        @brief Computes the distances between every couple of trace BBox.

        @:param bboxes A list of bboxes.
        @:param parallel Boolean indicating if the distance computing must be parallelized.
        @:param verbosity Level of verbosity. Default to 1.
        """
        log.debug(">> Compute the distance matrix between bounding boxes.")
        if len(bboxes) == 0:
            return np.array([])
        
        cust_dist = BBoxDistanceMatrix.generate_customized_distance_function(bb.BBox.distance_between_bboxes_coordinates, parallel = parallel)
        bboxes_coordinates = np.array([bbox.get_coordinates() for bbox in bboxes], ndmin=2)
        return cust_dist(bboxes_coordinates, bboxes_coordinates)


    @staticmethod
    def generate_customized_distance_function(kernel, parallel: bool = True):
        """
        Function used to speedup the complutation of the bbox distances with njit.
        
        @:param kernel: Name of the function to parallelized of speedup.
        @:param parallel: Boolean indicating if the distance computing must be parallelized.
        """
        kernel_numba = nb.njit(kernel, fastmath=True)
        def customized_dot_T(A,B):
            assert B.shape[1]==A.shape[1]
            out=np.empty((A.shape[0],B.shape[0]),dtype=np.float64)
            for i in nb.prange(A.shape[0]):
                for j in range(B.shape[0]):
                    out[i,j]=kernel_numba(A[i,:],B[j,:])
            return out
        return nb.njit(customized_dot_T, fastmath=True, parallel=parallel)
