import time
import logging
from typing import List, Dict

import models.driver as drv
import models.ride as rd
import algorithm.bbox_clusterizer as bc
import models.cluster as cl


class RidesClusterizer:
    """
    @brief Class that clusters a given set of Rides.
    """
    def __init__(self, distance_threshold: int = 10000, size_cluster_max: int = 150) -> None:
        """
        @brief Creates a new RideManager object that stores the

        @:param distance_threshold: The maximal number of rides per generated cluster. Default to 100000.
        @:param size_cluster_max: Path to the directory in which are the traces CSV files we want to parse. Default to 150.
        """
        self.distance_threshold = distance_threshold
        self.size_cluster_max = size_cluster_max


    def clusterize(self, rides: List[rd.Ride], log: logging.Logger) -> List[cl.Cluster]:
        """
        @brief Clusterizes the given rides and returns the clusters. 
        First clusterizes the drivers and then the rides per driver.

        @:param rides: List of rides to clusterize.
        @:param log: Logger to output informations.
        """
        start_time = time.time()
        log.info("> get_drivers")
        drivers = RidesClusterizer.get_drivers(rides=rides, log=log)
        log.info("--- get_drivers %s seconds ---" % (time.time() - start_time))

        start_time = time.time()
        log.info("> __clusterize_drivers")
        drivers_clusters = self.__clusterize_drivers(
            drivers=drivers,
            log=log)
        log.info("--- __clusterize_drivers %s seconds ---" % (time.time() - start_time))

        start_time = time.time()
        log.info("> __clusterize_rides_from_drivers_clusters")
        rides_clusters = self.__clusterize_rides_from_drivers_clusters(
            driver_clusters=drivers_clusters,
            drivers=drivers,
            log=log)
        log.info("--- __clusterize_rides_from_drivers_clusters %s seconds ---" % (time.time() - start_time))

        return rides_clusters


    @staticmethod
    def get_drivers(rides: List[rd.Ride], log: logging.Logger) -> List[drv.Driver]:
        """
        Returns a list of drivers corresponding to the given rides.
        
        :param rides: List of rides for which we want to know the drivers.
        :param log: Logger to output informations.
        """
        drivers = {}
        log.debug(">> Group rides by driver")
        for ride in rides:
            driver = drv.Driver(id = ride.driver_id)
            if ride.driver_id in drivers.keys():
                driver = drivers[ride.driver_id]
            driver.add_ride(ride)
            drivers[ride.driver_id] = driver
        return list(drivers.values())


    def __clusterize_drivers(self, drivers: List[drv.Driver], log: logging.Logger) -> List[cl.Cluster]:
        """
        Clusterize the given drivers.

        @:param drivers: List of drivers.
        @:param log: Logger to output informations.
        """
        bbox_clusterizer = bc.BboxClusterizer(distance_threshold=self.distance_threshold,
                                              size_cluster_max=float("inf"))
        return bbox_clusterizer.fit(bbox_containers=drivers, parallel=True, log=log)


    def __clusterize_rides_from_drivers_clusters(self, driver_clusters: List[cl.Cluster], drivers: Dict[int, drv.Driver], log: logging.Logger) -> List[cl.Cluster]:
        """
        Iterate over all driver clusters: find rides linked to the cluster and clusterize them.

        @:param clusters: List of cluster.
        @:param drivers: List of drivers.
        @:param log: Logger to output informations.
        """
        rides_clusters = []

        for cluster in driver_clusters:
            bbox_clusterizer = bc.BboxClusterizer(distance_threshold=self.distance_threshold,
                                                  size_cluster_max=self.size_cluster_max)
            driver_rides = [ride for bbox_container in cluster.bbox_containers for ride in bbox_container.rides]
            clusters = bbox_clusterizer.fit(bbox_containers=driver_rides,
                                            first_cluster_id=len(rides_clusters),
                                            log=log)
            rides_clusters = rides_clusters + clusters
        return rides_clusters
