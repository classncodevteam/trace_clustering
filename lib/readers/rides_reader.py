import os
import re
import logging
from typing import List
import pandas as pd

import models.ride as rd
import models.driver as dv


class RidesReader:
    """
    @brief Class that reads a CSV file with the rides matadatas and generates a vector of 
    rides objects from them.
    """
    def __init__(self, file: str, log: logging.Logger) -> None:
        """
        @brief Creates a new RidesReader object
        
        @:param file Path to the file in which are summarized the rides.
        @:param log Logger to output information.
        """
        if not os.path.isfile(file):
            log.error("The rides file " + file + " does not exist.")
            raise ValueError("The rides file " + file + " does not exist.")
        self.file = file

    def read(self, log: logging.Logger) -> List[rd.Ride]:
        """
        Reads the files and populates the rides data structures.

        @:param log Logger to output information.
        """
        log.debug(">> Reading file")

        rides = []
        rides_df = pd.read_csv(
            self.file, 
            sep=';')
        rides_df = rides_df.dropna()
        # TODO : remove
        # rides_df = rides_df.sample(300)
        for i,ride_row in rides_df.iterrows():
            ride = rd.Ride(driver_id = int(ride_row['driver_id']),
                           id = int(ride_row['id']),
                           lat_max = float(ride_row['max_lat']),
                           lat_min = float(ride_row['min_lat']),
                           lng_max = float(ride_row['max_lng']),
                           lng_min = float(ride_row['min_lng']))
            if ride.bbox.empty():
                continue
            rides.append(ride)
        return rides