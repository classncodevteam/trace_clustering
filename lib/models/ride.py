import pandas as pd
import models.bbox_container as co
import models.bbox as bb


class Ride(co.BBoxContainer):
    """
    @brief Ride is a bboxContainer represented by its bbox and driver_id.
    """
    def __init__(self, id: int, driver_id: int, lat_max: float, lat_min: float, lng_max: float, lng_min: float) -> None:
        """
        @brief Inits a ride with the given bbox coordinates and ids.

        @:param id Id of the newly created ride.
        @:param driver_id Id of the ride's driver.
        @:param lat_max Max latitude of the ride's bbox.
        @:param lat_min Min latitude of the ride's bbox.
        @:param lng_max Max longitude of the ride's bbox.
        @:param lng_min Min longitude of the ride's bbox.
        """
        co.BBoxContainer.__init__(
            self,
            id=id, 
            bbox=bb.BBox(lat_max=lat_max,
                         lat_min=lat_min,
                         lng_max=lng_max,
                         lng_min=lng_min,
                         id=driver_id)) # Id used to check the driver's id from the bbox
        self.id = id                 
        self.driver_id = driver_id
