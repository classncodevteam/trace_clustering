import math
import numpy as np
from numba import njit
import generategraph.area as ar
import utils.utils as utils
import algorithm.bbox_njit_utils as bbox_njit_utils


class BBox(ar.Area):
    """
    @brief Geographical area represented as a BBox (whose faces are determined by (min_lon, min_lat, max_lon, max_lat)).
    """

    def __init__(self, lat_max: float = 0.0, lat_min: float = 0.0, lng_max: float = 0.0, lng_min: float = 0.0, id: int = 0):
        """
        @brief Creates a new BBox object. Possibility to give it an id.

        @:param lat_max Value of the maximal latitude of the bbox. Default to 0.
        @:param lat_min Value of the minimal latitude of the bbox. Default to 0.
        @:param lng_max Value of the maximal longitude of the bbox. Default to 0.
        @:param lng_min Value of the minimal longitude of the bbox. Default to 0.
        @:param id Id of the BBox. Default to 0.
        """
        ar.Area.__init__(self, lat_max=lat_max, lat_min=lat_min, lng_max=lng_max, lng_min=lng_min, name=id)

    def id(self) -> int:
        """
        @brief Returns the id of the bbox
        """
        return self.name

    def get_coordinates(self) -> float :
        """
        @brief Returns the coordinates as a numpy array.
        """
        return np.array([float(self.lat_max), float(self.lat_min), float(self.lng_max), float(self.lng_min)])
    
    def get_area(self) -> float :
        """
        @brief Computes the area of the bbox.
        """
        return bbox_njit_utils.get_bbox_area(self.get_coordinates())

    def intersection(self, bbox: "BBox") -> "BBox":
        """
        @brief Computes the intersection between this bbox and another one.

        @:param bbox The other bbox.
        """
        intersection = bbox_njit_utils.get_bboxes_intersection(self.get_coordinates(), bbox.get_coordinates())
        return BBox(lat_max=intersection[0],
                    lat_min=intersection[1],
                    lng_max=intersection[2],
                    lng_min=intersection[3])


    def distance_to(self, bbox2: "BBox") -> float:
        """
        @brief Computes the distance from this BBox to another given BBox.

        @:param bbox2 The other BBox.
        """
        return BBox.distance_between_bboxes_coordinates(self.get_coordinates(), bbox2.get_coordinates())


    def empty(self):
        """
        Checks if the bbox is empty.
        """
        return self.lat_max == self.lat_min or self.lng_max == self.lng_min

    def __str__(self):
        return f"[{self.lat_min}, {self.lng_min}, {self.lat_max}, {self.lng_max}]"


    @staticmethod
    def distance_between_bboxes_coordinates(bbox1: np.array, bbox2: np.array) -> float:
        """
        @brief Computes the distance between two BBoxes.

        @:param bbox1 The first BBox.
        @:param bbox2 The second BBox.
        """
        lat_centroid_1 = bbox1[1] + (bbox1[0] - bbox1[1]) / 2
        lng_centroid_1 = bbox1[3] + (bbox1[2] - bbox1[3]) / 2
        lat_centroid_2 = bbox2[1] + (bbox2[0] - bbox2[1]) / 2
        lng_centroid_2 = bbox2[3] + (bbox2[2] - bbox2[3]) / 2
        centroid_distance = bbox_njit_utils.haversine(lat_centroid_1, lng_centroid_1,
                                                      lat_centroid_2, lng_centroid_2)
        return bbox_njit_utils.get_intersection_distance_between_bboxes(bbox1, bbox2) * centroid_distance


    @staticmethod
    def merge_bboxes(bbox1: "BBox", bbox2: "BBox") -> "BBox":
        """
        @brief Merges two bboxes. If one of the bboxes is None, returns the other one.

        @:param bbox1 The first BBox.
        @:param bbox2 The second BBox.
        """
        if not bbox1:
            return bbox2
        if not bbox2:
            return bbox1
        if bbox1.id() != bbox2.id():
            return bbox1
        lat_max = max(bbox1.lat_max, bbox2.lat_max)
        lat_min = min(bbox1.lat_min, bbox2.lat_min)
        lng_max = max(bbox1.lng_max, bbox2.lng_max)
        lng_min = min(bbox1.lng_min, bbox2.lng_min)
        return BBox(lat_max=lat_max,
                       lat_min=lat_min,
                       lng_max=lng_max,
                       lng_min=lng_min,
                       id=bbox1.id())
