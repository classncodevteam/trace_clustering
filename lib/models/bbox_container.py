from typing import List

import models.bbox as bb


class BBoxContainer:
    """
    @brief Object containing a bbox that can be clusterized. 
    """

    def __init__(self, id: int, bbox: bb.BBox) -> None:
        """
        @brief Inits an object containing a bbox.

        @:param id Id of the newly created bbox_container.
        @:param bbox A bbox.
        """
        self.id = id
        self.bbox = bbox
