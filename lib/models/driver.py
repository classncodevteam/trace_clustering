from typing import List

import models.ride as rd
import models.bbox as bb
import models.bbox_container as co


class Driver(co.BBoxContainer):
    """
    @brief Driver that contains a set of rides.
    """

    def __init__(self, id: int) -> None:
        """
        @brief Inits a driver with an empty list of rides with the given id.

        @:param id Id of the newly created driver.
        """
        co.BBoxContainer.__init__(self, id=id, bbox=None)
        self.rides = []

    def add_ride(self, ride: rd.Ride) -> None:
        """
        @brief Adds a new ride to the driver.

        @:param ride The rides one wants to add to the driver.
        """
        self.rides.append(ride)
        self.bbox = bb.BBox.merge_bboxes(bbox1=self.bbox, bbox2=ride.bbox)
