import os
import models.bbox as bb
import models.bbox_container as bc
import utils.utils as utils

from db import RideSql, ClusterSql

class Cluster:
    """
    @brief Cluster regrouping a set of bbox containers.
    """

    def __init__(self, id) :
        """
        @brief Inits a empty cluster of bboxes with the given id.

        @:param id Id of the newly created bbox.
        """
        self.id = id
        self.bbox = None
        self.bbox_containers = []

    def lat_max():
        return self.bbox.lat_max
        
    def lat_min():
        return self.bbox.lat_min
        
    def lng_max():
        return self.bbox.lng_max
        
    def lng_min():
        return self.bbox.lng_min
        
    def add_bbox_container(self, bbox_container):
        """
        @brief Adds a new bbox_container to the cluster.

        @:param bbox_container The bbox_container one wants to add to the cluster.
        """
        self.bbox_containers.append(bbox_container)
        new_lat_max = bbox_container.bbox.lat_max
        new_lat_min = bbox_container.bbox.lat_min
        new_lng_max = bbox_container.bbox.lng_max
        new_lng_min = bbox_container.bbox.lng_min
        if self.bbox != None:
            new_lat_max = max(self.bbox.lat_max, bbox_container.bbox.lat_max)
            new_lat_min = min(self.bbox.lat_min, bbox_container.bbox.lat_min)
            new_lng_max = max(self.bbox.lng_max, bbox_container.bbox.lng_max)
            new_lng_min = min(self.bbox.lng_min, bbox_container.bbox.lng_min)
        self.bbox = bb.BBox(lat_max=new_lat_max,
                            lat_min=new_lat_min,
                            lng_max=new_lng_max,
                            lng_min=new_lng_min,
                            id=self.id)

    def set_cluster_id_on_rides(self):
        new_id = ClusterSql.create_new_cluster(self)
        RideSql.set_cluster_ids(self.bbox_containers, new_id)

    def __str__(self):
        return f"[{self.id}, {self.bbox}, {len(self.bbox_containers)}]"