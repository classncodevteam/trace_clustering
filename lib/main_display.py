import time, random
import logging
import readers.rides_reader as rr
import argparse

import algorithm.rides_clusterizer as rc
import writers.clusters_writer as cw
from db.ride_sql import RideSql
from datetime import datetime, date
logging.getLogger('numba').setLevel(logging.CRITICAL)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=('Geographical clustering of a set of courses into subareas.'))

    # INPUT
    parser.add_argument("-t", "--date", type=str, nargs='?', default=str(date.today()), help=("Maximum number of rides per generated cluster."))
    parser.add_argument("-s", "--size_cluster_max", type=int, nargs='?', default=150, help=("Maximum number of rides per generated cluster."))
    parser.add_argument("-d", "--distance_threshold", type=float, nargs='?', default=10000, help=("Path to the directory in which can be found the map matched courses. All the "
                                                                                "csv and txt files in this directory will be taken into account.")) # with courses
    parser.add_argument("-v", "--verbosity", help="Output verbosity.")
    args = parser.parse_args()
    date = datetime.strptime(args.date, '%Y-%m-%d')

    # LOGGING
    logging.basicConfig(level=logging.DEBUG)
    log = logging.getLogger()

    start_time = time.time()
    log.info("READ FILES")
    #rides_reader = rr.RidesReader(file=args.file, log=log)
    #rides = rides_reader.read(log=log)
    rides = RideSql.get_rides_for_day(date)
    log.info("rides: %d" %len(rides))
    log.info("--- read %d rides in %s seconds ---" % (len(rides), (time.time() - start_time)))

    start_time = time.time()
    log.info("CLUSTERING")
    rides_clusterizer = rc.RidesClusterizer(distance_threshold = args.distance_threshold, size_cluster_max = args.size_cluster_max)
    clusters = rides_clusterizer.clusterize(rides=rides, log=log)
    log.info("--- clusterize %d clusters in %s seconds ---" % (len(clusters), (time.time() - start_time)))

    f = open('../display/js/data/bboxes.js', 'w')
    f.write('data = [')
    for c in clusters:
        f.write(f"{c},")
    f.write(']')
    f.close()
