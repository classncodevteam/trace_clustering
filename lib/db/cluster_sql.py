from db.pg_connection import PGConnection

class ClusterSql(PGConnection):
    @classmethod
    def create_new_cluster(cls, cluster):
        bbox = cluster.bbox
        cls.cursor.execute("INSERT INTO clusters (min_lat, min_lng, max_lat, max_lng) VALUES (%s, %s, %s, %s) RETURNING ID", (bbox.lat_min, bbox.lng_min, bbox.lat_max, bbox.lng_max))
        cls.connection.commit()
        return cls.cursor.fetchone()[0]