import psycopg2
import psycopg2.extras
import config.config as config

class PGConnection:
    if(config.RIDE_DATABASE_URL):
        connection = psycopg2.connect(config.RIDE_DATABASE_URL)
        cursor     = connection.cursor(cursor_factory = psycopg2.extras.DictCursor)