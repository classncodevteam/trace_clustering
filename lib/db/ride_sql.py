from db.pg_connection import PGConnection
from datetime import timedelta
from models.ride import Ride

class RideSql(PGConnection):
    @classmethod
    def get_rides_for_day(cls, date, all=False):
        begin_date = date.strftime('%Y-%m-%dT%H:%M:%S')
        end_date = (date + timedelta(days=1)).strftime('%Y-%m-%dT%H:%M:%S')
        query = "SELECT * from rides where ride_begin BETWEEN %s AND %s"
        if not all:
            query += " AND \"cluster_id\" IS NULL"

        cls.cursor.execute(query, (begin_date, end_date))
        rides_records = cls.cursor.fetchall()
        return [
            Ride(
                id=ride_record['id'], 
                driver_id=ride_record['driver_id'], 
                lat_max=ride_record['max_lat'],
                lat_min=ride_record['min_lat'], 
                lng_max=ride_record['max_lng'], 
                lng_min=ride_record['min_lng']
            ) 
            for ride_record in rides_records
        ]

    @classmethod
    def set_cluster_ids(cls, rides, cluster_id):
        cls.cursor.execute("UPDATE rides SET cluster_id = %s WHERE id IN %s", (cluster_id, tuple([r.id for r in rides])))
        cls.connection.commit()