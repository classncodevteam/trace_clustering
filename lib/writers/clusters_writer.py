import os 
import logging
from typing import List
import plotly.graph_objects as go

import models.cluster as cl
import utils.utils as utils


class ClustersWriter():
    @staticmethod
    def write(clusters: List[cl.Cluster], filename: str, log: logging.Logger) -> None:
        """
        @brief Writes the clusters in a output csv fle.

        @:param clusters List of clusters.
        @:param filename Path to the outputed csv file.
        @:param log Logger to output informations.
        """
        utils.create_directory(directory=os.path.dirname(filename), log=log)
        try :
            with open(filename, 'w') as file:
                file.write("ride_id;cluster_id\n")
                for cluster in clusters:
                    for ride in cluster.bbox_containers:
                        file.write("%d;%d\n" %(ride.id, cluster.id))
                log.info("Outputed clusters in the file " + filename)
        except Exception as _ :
            raise Exception("Problem encountered while outputing clusters in file " + filename)


    @staticmethod
    def plot(clusters: List[cl.Cluster], filename: str, log: logging.Logger, show_plot: bool = True) -> None:
        """
        @brief Plots the clusters.

        @:param clusters List of clusters.
        @:param filename Path to the outputed plot.
        @:param show_plot Boolean that indicates if the cluster figure should be ploted. Default to True.
        @:param log Logger to output informations.
        """
        data = []
        colors = ["coral", "chartreuse", "yellow", "olive", "teal", "aqua", "tomato", "olive", "skyblue", "indigo", "magenta", "pink"]
        for cluster in clusters:
            color_id = int(cluster.id % len(colors))
            color = colors[color_id]
            if cluster.id == -1:
                color = "black"
            for i, bbox_container in enumerate(cluster.bbox_containers):
                data.append(
                    dict(
                        type = 'scattermapbox',
                        mode = 'markers+lines',
                        lat = [bbox_container.bbox.lat_min, bbox_container.bbox.lat_min, 
                            bbox_container.bbox.lat_max, bbox_container.bbox.lat_max, 
                            bbox_container.bbox.lat_min],
                        lon = [bbox_container.bbox.lng_max, bbox_container.bbox.lng_min, 
                            bbox_container.bbox.lng_min, bbox_container.bbox.lng_max, 
                            bbox_container.bbox.lng_max],
                        fill = 'toself',
                        marker = {'color': color},
                        hovertext="Ride id : %s\nLabel : %s"%(bbox_container.bbox.id(), cluster.id),
                        hoverinfo='text'
                    )
                )
        fig = go.Figure(data)
        fig.update_layout(
            hovermode='closest',
            mapbox = {
                'style': "stamen-terrain",
                'zoom': 1})
        utils.create_directory(directory=os.path.dirname(filename), log=log)
        fig.write_html(filename)
        if show_plot:
            fig.show()