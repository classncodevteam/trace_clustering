var markers_icons = [
    'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
    'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
    'http://maps.google.com/mapfiles/ms/icons/purple-dot.png',
    'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
    'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
];


function initData(){
    setMap();
}



function setMap() {
    $('<div />', { id: 'map', class: 'map' }).appendTo($('#simulations'));

    let map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 43.289518, lng: 5.407560},
        zoom: 12
    });

    for(let index in data) {
      let cluster = data[index];
      let bbox = cluster[1]
      let polygon = [
        { lat: bbox[0], lng: bbox[1] },
        { lat: bbox[0], lng: bbox[3] },
        { lat: bbox[2], lng: bbox[3] },
        { lat: bbox[2], lng: bbox[1] },
        { lat: bbox[0], lng: bbox[1] }
      ];

      // Construct the polygon.
      var bbox_map = new google.maps.Polygon({
        paths: polygon,
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map: map
      });
      bbox_map.addListener('click', () => {$('#current_date').html(`${cluster[0]}, ${cluster[2]}`)})
    }

      
}

